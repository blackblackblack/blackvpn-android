package de.blinkt.openvpn.Event;

import java.util.EventObject;

/**
 * Created by Dima on 17.11.2015.
 */
public class Event {
	private boolean isConnected = false;
	private String Error;

	public Event() {
		isConnected = false;

	}

	public String getError() {
		return Error;
	}

	public void setError(String error) {
		Error = error;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setIsConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}
}
