package de.blinkt.openvpn.Event;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

/**
 * Created by Dima on 17.11.2015.
 */
public class BusProvider extends Bus {
	private final Handler mHandler = new Handler(Looper.getMainLooper());
	private static final BusProvider busProvider = new BusProvider();

	public static BusProvider getInstance(){
		return busProvider;
	}
	@Override
	public void post(final Object event) {
		if (Looper.myLooper() == Looper.getMainLooper()) {
			super.post(event);
		} else {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					BusProvider.super.post(event);
				}
			});
		}
	}
}
