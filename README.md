# Black VPN for Android #

*Black VPN for Android* is an OpenVPN client to connect to the VPN service by BlackVPN.com

Easily an quickly connect to your favorite VPN locations including USA, UK (London), Australia, Brazil, Canada, Czech Republic, Estonia, France, Germany, Japan, Lithuania, Luxembourg, Netherlands, Norway, Romania, Spain, Switzerland and Ukraine

Connect using the most secure VPN protocol - OpenVPN - using AES-256-CBC encryption with 4096 bit keys.

# Downloads #

Available worldwide on the [Play Store](https://play.google.com/store/apps/details?id=com.blackvpn).

Binary .APK files can also be downloaded directly from the [BlackVPN website](https://www.blackvpn.com/setup/android/blackvpn-app/)

# Build Instructions #

## Prerequisites ##
* [Android Studio and Android SDK tools](http://developer.android.com/sdk/index.html) for Windows, Mac OS X and Linux

## Building From Source ##
* git clone https://bitbucket.org/blackblackblack/blackvpn-android.git
* Build -> Make Project
* Build -> Build APK

# Credits #

*Black VPN for Android* uses [ics-openvpn](https://github.com/schwabe/ics-openvpn) for OpenVPN connectivity, which is licensed under GPLv2.

# License #

*Black VPN for Android* (com.blackvpn) is licensed under GPLv2.  For more information see the [doc/COPYING.txt](https://bitbucket.org/blackblackblack/blackvpn-android/src/master/doc/COPYING.txt) and [doc/LICENSE.txt](https://bitbucket.org/blackblackblack/blackvpn-android/src/master/doc/LICENSE.txt) files.

Copyright (c) 2016, BLACKVPN LIMITED (Hong Kong). All rights reserved.