package com.blackvpn;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.blackvpn.Events.BusProvider;
import com.blackvpn.Events.EventMessage;
import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {
    private static final String TAG = "com.blackvpn";
    private String topic = "global";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String token = null;

        try {
            synchronized (TAG) {

                String uniquePhoneId = android.os.Build.SERIAL;
                if (uniquePhoneId == null) {
                    Settings.Secure.getString(getApplicationContext().getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                }


                // Initially a network call, to retrieve the token, subsequent calls are local.
                InstanceID instanceID = InstanceID.getInstance(this);
                token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                Log.d(Const.LOG, "RegistrationIntentService.class -> we got GCM Registration Token: " + token);

                sendRegistrationToServer(token, uniquePhoneId);


                Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
                prefs.setGcmToken(token);
            }
        } catch (Exception e) {
            Log.d(Const.LOG, "RegistrationIntentService.class -> Failed to complete token refresh", e);
            BusProvider.getInstance().post(new EventMessage(Const.GCM_ERROR));

        }

        Log.d(Const.LOG, "RegistrationIntentService.class -> onHandleIntent : end");

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        if (token != null) {
            BusProvider.getInstance().post(new EventMessage(Const.GCM_UPDATE_TOKEN, token));
        }

    }

    /**
     * Register a GCM registration token with the app server
     * @param token Registration token to be registered
     * @param string_identifier A human-friendly name for the client
     * @throws IOException
     */
    private void sendRegistrationToServer(String token, String string_identifier) throws IOException {

        Log.d(Const.LOG, "RegistrationIntentService.class -> sendRegistrationToServer()");
        Log.d(Const.LOG, "token = " + token);
        Log.d(Const.LOG, "string_identifier = " + string_identifier);

        Bundle registration = createRegistrationBundle(token, string_identifier);

        String senderId = getString(R.string.gcm_defaultSenderId);
        GoogleCloudMessaging.getInstance(this).send(senderId + "@gcm.googleapis.com",
                String.valueOf(System.currentTimeMillis()), registration);
    }

    /**
     * Creates the registration bundle and fills it with user information
     * @param token Registration token to be registered
     * @param string_identifier A human-friendly name for the client
     * @return A bundle with registration data.
     */
    private Bundle createRegistrationBundle(String token, String string_identifier) {
        Bundle registration = new Bundle();

        // Create the bundle for registration with the server.
        registration.putString("action", "register_new_client");
        registration.putString("registration_token", token);
        registration.putString("stringIdentifier", string_identifier);
        return registration;
    }
}
