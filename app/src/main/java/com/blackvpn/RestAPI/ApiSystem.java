package com.blackvpn.RestAPI;

import com.blackvpn.Utils.Const;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 *
 * Created by grow on 20.11.15.
 */
public class ApiSystem {

    public interface TicketPost {
        @Headers("X-API-Key: " + Const.TICKET_API_KEY)
        @POST("/tickets.json")
        void createTicket(@Body ModelTicket body, Callback<String> callback);
    }
}
