package com.blackvpn.RestAPI;

import com.google.gson.annotations.SerializedName;

public class Attachments {
    @SerializedName("file.txt")
    String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

}
