package com.blackvpn.RestAPI;

import android.util.Log;

import com.blackvpn.Utils.Const;

import java.util.ArrayList;

public class ModelTicket {

    private boolean alert;

    private boolean autorespond;

    private String source;

    private String name;

    private String email;

    private String phone;

    private String subject;

    private String ip;

    private String message;

    private ArrayList<Attachments> attachments = new ArrayList<>();

    public void setAttachmentUTF8(String fileTextContents) {
        Log.d(Const.LOG, "Decoded value is " + fileTextContents);
        Attachments attachments = new Attachments();
        attachments.setFile("data:text/plain;charset=utf-8," + fileTextContents);
        this.attachments.add(attachments);
    }

    public boolean getAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public boolean getAutorespond() {
        return autorespond;
    }

    public void setAutorespond(boolean autorespond) {
        this.autorespond = autorespond;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
