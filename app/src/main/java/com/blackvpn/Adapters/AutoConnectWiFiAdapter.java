package com.blackvpn.Adapters;

import android.graphics.drawable.Drawable;
import android.net.wifi.WifiConfiguration;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * List of known WiFi networks
 *
 * @author Andrew.Gahov@gmail.com  (11.05.17)
 */

public class AutoConnectWiFiAdapter extends RecyclerView.Adapter<AutoConnectWiFiAdapter.ViewHolder> {

    private List<WifiConfiguration> wifiListFromDevice;
    private ArrayList<String> checkedKnownNetworks;

    public AutoConnectWiFiAdapter(List<WifiConfiguration> wifiConfigurationList) {
        if (wifiConfigurationList == null) {
            wifiConfigurationList = new ArrayList<>();
        }
        wifiListFromDevice = wifiConfigurationList;
        checkedKnownNetworks = BlackVpnSystem.getInstance().getListOfEnabledKnownWiFi();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_autoconnect_wifi_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        WifiConfiguration wifi = wifiListFromDevice.get(position);

        String wifiTitle = wifi.SSID.replaceAll("\"","");
        viewHolder.tvTitle.setText(wifiTitle);

        Drawable background = position % 2 == 0 ? viewHolder.gray1 : viewHolder.gray2;

        final String wifiKey = BlackVpnSystem.getInstance().getWiFiKey(wifi);

        // set background
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            viewHolder.rlItem.setBackground(background);
        } else{
            viewHolder.rlItem.setBackgroundDrawable(background);
        }

        // make checked position
        viewHolder.switchWifi.setChecked(checkedKnownNetworks.contains(wifiKey));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {

                // Handle click on view
                boolean isChecked = !viewHolder.switchWifi.isChecked();
                viewHolder.switchWifi.setChecked(isChecked);
                if (isChecked) {
                    if (!checkedKnownNetworks.contains(wifiKey)) {
                        checkedKnownNetworks.add(wifiKey);
                    }
                } else {
                    if (checkedKnownNetworks.contains(wifiKey)) {
                        checkedKnownNetworks.remove(wifiKey);

                    }
                }
                // Save changed list of known wifi networks
                BlackVpnSystem.getInstance().setListOfEnabledKnownWiFi(checkedKnownNetworks);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wifiListFromDevice.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindDrawable(R.color.autoconnect_line1)
        Drawable gray1;
        @BindDrawable(R.color.autoconnect_line2)
        Drawable gray2;

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.switchWifi)
        SwitchCompat switchWifi;
        @BindView(R.id.rlItem)
        RelativeLayout rlItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
