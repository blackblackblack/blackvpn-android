package com.blackvpn.Adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackvpn.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * There is a Spinner list to the AutoConnectActivity
 *
 * @author Andrew.Gahov@gmail.com  (11.05.17)
 */

public class AutoConnectSpinnerAdapter extends ArrayAdapter<String> {

    private final int normalLayout;
    private final int dropDownLayout;

    public AutoConnectSpinnerAdapter(@NonNull Context context, @NonNull ArrayList<String> arrayList,
                                     @LayoutRes int normalLayout, @LayoutRes int dropDownLayout) {
        super(context, normalLayout, R.id.tvItem, arrayList);

        this.normalLayout = normalLayout;
        this.dropDownLayout = dropDownLayout;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(dropDownLayout, parent,
                    false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        // set values:
        holder.tvItem.setText(getItem(position));


        // if it is the last position -> remove the divider
        boolean hideDivider = position + 1 == getCount();
        holder.llDivider.setVisibility(hideDivider ? View.GONE : View.VISIBLE);

        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(normalLayout, parent,
                    false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.tvItem.setText(getItem(position));

        return convertView;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }


    /*
        Important! Your layout must contains the TextView with id "R.id.tvItem".
     */
    static class ViewHolder {

        @BindView(R.id.tvItem)
        TextView tvItem;
        @BindView(R.id.llDivider)
        LinearLayout llDivider;

        ViewHolder(View view) {
            ButterKnife.bind(ViewHolder.this, view);
        }
    }
}
