package com.blackvpn.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blackvpn.Application;
import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Prefs;
import com.blackvpn.Utils.Server;

import java.util.HashMap;

import de.blinkt.openvpn.VpnProfile;

public class AllVPNsAdapter extends BaseAdapter {
    private final Prefs mPrefs;
    private final Resources mRes;
    private final Server.VPN[] mServersList;
    private final HashMap<String, VpnProfile> mProfiles;
    private final AppCompatActivity mActivity;

    private static class ViewHolder {
        Button btnCountryName;
        ImageView ivLogo;
        LinearLayout llMain;
        CheckBox checkBox;
    }

    public AllVPNsAdapter(AppCompatActivity activity, Prefs prefs, Resources res) {
        mPrefs = prefs;
        mRes = res;
        mServersList = Server.getSortedList(mRes);
        mProfiles = BlackVpnSystem.getInstance().getMapAllProfiles();
        mActivity = activity;
    }


    //Return amount of countries from list
    @Override
    public int getCount() {
        return mServersList.length;
    }


    //Return profile by position
    @Override
    public VpnProfile getItem(int position) {
        return mProfiles.get(mServersList[position].getProfileKey());
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Server.VPN server = mServersList[position];
        String countryName = mRes.getString(server.getTitle());

        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(Application.getContext()).inflate(R.layout.allvpns_adapter,
                    parent, false);
            holder.checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            holder.btnCountryName = (Button) view.findViewById(R.id.btnCountryName);
            holder.ivLogo = (ImageView) view.findViewById(R.id.ivLogo);
            holder.llMain = (LinearLayout) view.findViewById(R.id.llMain);
            view.setTag(holder);
        }
        holder = (ViewHolder) view.getTag();
        holder.checkBox.setChecked(Server.isServerFavorite(server));
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!buttonView.isShown()) {
                    return;
                }
                Server.setFavoriteServer(server, isChecked);
                notifyDataSetChanged();
            }
        });

        holder.btnCountryName.setText(countryName);
        holder.btnCountryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPrefs.setLastServer(server);
                Intent intent = new Intent();
                intent.putExtra(Server.PROFILE_KEY, server.getProfileKey());
                mActivity.setResult(Activity.RESULT_OK, intent);
                mActivity.finish();

            }
        });

        holder.ivLogo.setImageResource(server.getServerIcon());

        return view;
    }
}
