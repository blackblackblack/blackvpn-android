package com.blackvpn.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackvpn.Application;
import com.blackvpn.Utils.Server;

import java.util.ArrayList;

public class FavoriteAdapter extends BaseAdapter {

    private final ArrayList<Server.VPN> mFavorites;
    private Activity mActivity;

    private static class ViewHolder {
        TextView tvCountryName;
    }

    public FavoriteAdapter(Activity activity) {
        mActivity = activity;
        mFavorites = Server.getFavoritesList();
    }


    @Override
    public int getCount() {
        return mFavorites.size();
    }


    @Override
    public Server.VPN getItem(int position) {
        return mFavorites.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getDropDownView(position, convertView, parent);
    }


    //The layout for dropdown list
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Server.VPN server = getItem(position);

        View view = convertView;
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(Application.getContext()).inflate(
                    android.R.layout.simple_spinner_dropdown_item, parent, false);
            viewHolder.tvCountryName = (TextView) view.findViewById(android.R.id.text1);
            view.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) view.getTag();

        viewHolder.tvCountryName.setText(mActivity.getString(server.getTitle()));
        return view;
    }
}
