package com.blackvpn;

import android.content.Intent;
import android.util.Log;

import com.blackvpn.Utils.Const;
import com.google.android.gms.iid.InstanceIDListenerService;

public class MyInstanceIDListenerService extends InstanceIDListenerService {
    /**
     * Called if InstanceID token is updated.
     * May occur if the security of the previous token had been compromised
     * and is initiated by the InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {
        Log.d(Const.LOG, "MyInstanceIDListenerService.class -> onTokenRefresh()");

        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        startService(new Intent(this, RegistrationIntentService.class));
    }
}
