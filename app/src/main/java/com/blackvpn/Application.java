package com.blackvpn;

import android.content.Context;

import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Prefs;
import com.blackvpn.Utils.Server;

import de.blinkt.openvpn.core.PRNGFixes;

public class Application extends android.app.Application {

    private static Context mContext;

    @Override
	public void onCreate() {
		super.onCreate();
        PRNGFixes.apply();
        mContext = getApplicationContext();
        BlackVpnSystem.getInstance().migrate();
    }

    public static Context getContext() {
        return mContext;
    }

}
