package com.blackvpn.Activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Helpers.BackgroundHelper;
import com.blackvpn.Utils.Prefs;

import java.util.Locale;

public class ProtocolActivity extends AppCompatActivity {
    private Prefs prefs;

    private CoordinatorLayout clProtocolActivity;
    private TextView etPortNumber;

    private View thumbLeft;
    private View thumbRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protocol);
        BlackVpnSystem system = BlackVpnSystem.getInstance();
        prefs = system.getPrefs();
        setUI();
    }

    private void checkLocale() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        android.content.res.Configuration conf = getResources().getConfiguration();
        conf.locale = Locale.getDefault();
        getResources().updateConfiguration(conf, displayMetrics);
    }

    private void setUI() {
        setToolbar();
        clProtocolActivity = (CoordinatorLayout) findViewById(R.id.clProtocolActivity);
        RelativeLayout aSwitch = (RelativeLayout) findViewById(R.id.protocol_switch);
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (thumbLeft.getVisibility() == View.VISIBLE
                        && thumbRight.getVisibility() == View.GONE) {
                    setChecked(Const.TCP);
                    prefs.setProtocol(Const.TCP);
                } else {
                    setChecked(Const.UDP);
                    prefs.setProtocol(Const.UDP);
                }
            }
        });
        thumbLeft = findViewById(R.id.switch_thumb_left);
        thumbRight = findViewById(R.id.switch_thumb_right);
        etPortNumber = (TextView) findViewById(R.id.input_port);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
    }

    private void initUI() {
        checkLocale();

        String port = prefs.getPort();

        if (port != null && !port.isEmpty()) {
            etPortNumber.setText(port);
        } else {
            etPortNumber.setText(Const.DEFAULT_PORT);
        }

        setChecked(prefs.getProtocol());

        clProtocolActivity.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: //back pressed in toolbar
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setChecked(boolean isChecked) {
        if (isChecked) {
            thumbLeft.setVisibility(View.GONE);
            thumbRight.setVisibility(View.VISIBLE);
        } else {
            thumbLeft.setVisibility(View.VISIBLE);
            thumbRight.setVisibility(View.GONE);
        }
    }

}
