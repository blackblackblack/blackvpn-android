package com.blackvpn.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.blackvpn.Adapters.FavoriteAdapter;
import com.blackvpn.Application;
import com.blackvpn.Events.BusProvider;
import com.blackvpn.Events.EventErrorVpn;
import com.blackvpn.Events.EventUpdateVpnState;
import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Helpers.BackgroundHelper;
import com.blackvpn.Utils.Helpers.StartHelper;
import com.blackvpn.Utils.Prefs;
import com.blackvpn.Utils.Server;
import com.blackvpn.Utils.VPNwork.Connect;
import com.blackvpn.Utils.VPNwork.Disconnect2;
import com.blackvpn.Utils.WriteLogs;
import com.squareup.otto.Subscribe;

import java.util.HashMap;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.activities.ConfigConverter;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VpnStatus;

public class MainActivity extends AppCompatActivity implements BackgroundHelper.BackgroundForServer {

    //Defining variable names
    private BlackVpnSystem system;
    private BusProvider busProvider;
    public FavoriteAdapter favoriteAdapter;
    private Prefs prefs;

    private boolean isToastPossibleToShow = true;
    private String vpnState;
    private Server.VPN mCurrentServer;

    private PopupMenu popupMenu;
    private Dialog dialogFavorite;

    private CoordinatorLayout coordinatorMain;
    private LinearLayout ll_favorites;
    private TextView tvTitle;
    private ImageView ivConnectStatus;
    private TextView tvConnectStatus;

    private TextView mTitleOn;
    private TextView mTitleOff;
    private View mThumbLeft;
    private View mThumbRight;

    private LinearLayout aSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSystem();
        setUI();
    }

    private void setSystem() {

        //getSystem
        system = BlackVpnSystem.getInstance();
        //first init EventBus
        BlackVpnSystem.setVpnEvent(true);
        //get system Prefs
        prefs = system.getPrefs();
        //get VPN-state
        vpnState = prefs.getStatusVPN();
        //subscribe to the EventBus
        busProvider = BusProvider.getInstance();
        busProvider.register(this);

        final StartHelper startHelper = new StartHelper();
        startHelper.initSystem(this, system, this);

        //loading mapList of profiles
        startHelper.initProfiles();

        favoriteAdapter = new FavoriteAdapter(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
    }

    private void setUI() {
        aSwitch = (LinearLayout) findViewById(R.id.connect_switch);
        coordinatorMain = (CoordinatorLayout) findViewById(R.id.clMainActivity);
        mThumbLeft = findViewById(R.id.switch_thumb_left);
        mThumbRight = findViewById(R.id.switch_thumb_right);

        mTitleOn = (TextView) findViewById(R.id.title_switch_on);
        mTitleOff = (TextView) findViewById(R.id.title_switch_off);

        initListenerForSwitch();

        LinearLayout ll_all_vpn = (LinearLayout) findViewById(R.id.ll_all_vpn);
        ll_all_vpn.setOnClickListener(new AllVPNClickListener());
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        ll_favorites = (LinearLayout) findViewById(R.id.ll_favorites);

        initToolbar();
    }

    private void initListenerForSwitch() {
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mThumbLeft.getVisibility() == View.VISIBLE
                        && mThumbRight.getVisibility() == View.GONE) {
                    startVpnConnect(mCurrentServer == null ? Server.getDefaultServer() : mCurrentServer);
                    setCheckedThumb(true);
                } else {
                    disconnectVpn();
                    setCheckedThumb(false);
                }
            }
        });
    }

    private void setSpinner() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                dialogFavorite = null;
                favoriteAdapter = null;
                favoriteAdapter = new FavoriteAdapter(MainActivity.this);
                ll_favorites.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (favoriteAdapter.getCount() == 0) {
                            Intent intent = new Intent(MainActivity.this, AllVPNsActivity.class);
                            startActivityForResult(intent, Const.SELECT_SERVERS);
                        } else {
                            AlertDialog.Builder builderDialogFavorite = new AlertDialog.Builder(MainActivity.this);
                            builderDialogFavorite.setAdapter(favoriteAdapter, new FavoriteListClickListener());
                            dialogFavorite = builderDialogFavorite.create();
                            dialogFavorite.setCanceledOnTouchOutside(true);
                            dialogFavorite.show();
                        }
                    }
                });
            }
        });

    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_menu_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu = new PopupMenu(getApplicationContext(), v);
                popupMenu.inflate(R.menu.menu_main);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = system.onToolbarItemClick(MainActivity.this, item);
                        if (intent != null) {
                            startActivity(intent);
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        ivConnectStatus = (ImageView) toolbar.findViewById(R.id.ivConnectStatus);
        tvConnectStatus = (TextView) toolbar.findViewById(R.id.tvConnectStatus);
    }

    @Override
    protected void onDestroy() {
        busProvider.unregister(this);
        prefs.setStatusVPN(Const.VPN_STATE_NO_PROCESS);
        WriteLogs.unRegisterEventBus();
        super.onDestroy();
    }

    private void initUI() {
        system = BlackVpnSystem.getInstance();
        prefs = system.getPrefs();

        // set a current server
        mCurrentServer = prefs.getLastServer() == null ?
                Server.getDefaultServer() : prefs.getLastServer();

        setBackgroundForCurrentServer(coordinatorMain, this, mCurrentServer);

        tvTitle.setText(getString(mCurrentServer.getTitle()));

        //reload switch and status
        setConnectStatusAndThumb();

        //set spinner in last position
        setSpinner();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (system.appIsNowStarting()) {
            system.setNowAppIsStarting(false);

            if (prefs.isConnectOnAppStartUp() && Const.VPN_STATE_NO_PROCESS.equals(vpnState)) {
                autoConnect();
            }
        }

        if (system.isForceStartForKnownWiFi()) {
            system.setForceStartForKnownWiFi(false);

            if (Const.VPN_STATE_NO_PROCESS.equals(vpnState)) {
                autoConnect();
            }
        }
    }

    private void autoConnect() {
        Server.VPN vpn = prefs.getAutoConnectVpnLocation();
        if (vpn == null) vpn = Server.getDefaultServer();
        prefs.setLastServer(vpn);
        startVpnConnect(vpn);
    }

    /**
     * Logic of work of the switch
     */
    private void setCheckedThumb(Boolean checkedTitle) {
        if (checkedTitle) {

            if (mThumbLeft.getVisibility() == View.GONE
                    && mThumbRight.getVisibility() == View.VISIBLE) return;

            mThumbLeft.setVisibility(View.GONE);
            mThumbRight.setVisibility(View.VISIBLE);

            mTitleOff.setVisibility(View.INVISIBLE);
            mTitleOn.setVisibility(View.VISIBLE);

        } else {

            if (mThumbLeft.getVisibility() == View.VISIBLE
                    && mThumbRight.getVisibility() == View.GONE) return;

            mThumbLeft.setVisibility(View.VISIBLE);
            mThumbRight.setVisibility(View.GONE);

            mTitleOff.setVisibility(View.VISIBLE);
            mTitleOn.setVisibility(View.INVISIBLE);
        }

    }

    /**
     * Set status in the toolbar in a current value
     */
    private void setConnectStatusAndThumb() {
        vpnState = system.getPrefs().getStatusVPN();

        //check
        boolean isEmpty = TextUtils.isEmpty(vpnState);
        boolean unknown = Const.VPN_STATE_UNKNOWN_LEVEL.equals(vpnState);
        boolean noProcess = Const.VPN_STATE_NO_PROCESS.equals(vpnState);
        boolean cancelled = Const.VPN_STATE_NO_PROCESS.equals(vpnState);
        boolean permissionCancelled = Const.USER_VPN_PERMISSION_CANCELLED.equals(vpnState);
        boolean exiting = Const.EXITING.equals(vpnState);
        boolean connected = Const.VPN_STATE_CONNECTED.equals(vpnState);

        //set
        if (isEmpty || unknown || noProcess || cancelled || permissionCancelled || exiting) {
            //status = Disconnect

            if ((mThumbLeft.getVisibility() == View.GONE
                    && mThumbRight.getVisibility() == View.VISIBLE) &&
                    (tvConnectStatus.getText()
                            .equals(getResources().getString(R.string.not_connected)))) return;

            tvConnectStatus.setText(getResources().getString(R.string.not_connected));
            ivConnectStatus.setImageResource(R.drawable.halohen_red);

            setCheckedThumb(false);

        } else if (connected) {
            //status = Connect

            tvConnectStatus.setText(getResources().getString(R.string.connected));
            ivConnectStatus.setImageResource(R.drawable.halohen_green);

            setCheckedThumb(true);

        } else {
            //status = Connected...

            tvConnectStatus.setText(getResources().getString(R.string.connecting));
            ivConnectStatus.setImageResource(R.drawable.halohen_orange);

            setCheckedThumb(true);
        }
    }

    /**
     * start VpnConnect for a current profile
     */
    private void startVpnConnect(Server.VPN server) {

        mCurrentServer = server;
        tvTitle.setText(getString(mCurrentServer.getTitle()));

        isToastPossibleToShow = true;

        final Connect connect = new Connect(getApplicationContext());
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Const.Signal signal = connect.startVPN(mCurrentServer.getProfileKey(), MainActivity.this);

                if (Const.Signal.NoEmailOrPassword.equals(signal)) {
                    BusProvider.getInstance().post(new EventUpdateVpnState(Const.EXITING,
                            Const.EXECUTE_FORCE_EXIT, R.string.execute_force_exit,
                            VpnStatus.ConnectionStatus.LEVEL_AUTH_FAILED));

                    Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                    MainActivity.this.startActivity(intent);
                }
            }
        });
        thread.start();

        setConnectStatusAndThumb();
    }

    /**
     * will disconnect for a VPN services
     */
    private void disconnectVpn() {
        ProfileManager.setConntectedVpnProfileDisconnected(this);
        VpnStatus.clearLog();
        Disconnect2.start(this);

        setConnectStatusAndThumb();
    }

    public void createMapAllProfile() {
        for (Server.VPN server : Server.getAllVpnList()) {
            Uri uri = Uri.parse(server.getFilePath());
            Intent startImport = new Intent(MainActivity.this, ConfigConverter.class);
            startImport.setAction(ConfigConverter.IMPORT_PROFILE);
            startImport.setData(uri);
            startActivityForResult(startImport, Const.IMPORT_PROFILE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            setConnectStatusAndThumb();
            return;
        }

        switch (requestCode) {
            case Const.IMPORT_PROFILE:
                String profileUUID = data.getStringExtra(VpnProfile.EXTRA_PROFILEUUID);
                saveProfiles(ProfileManager.get(getApplicationContext(), profileUUID));
                break;

            case Const.SELECT_SERVERS:
                if (data != null && data.hasExtra(Server.PROFILE_KEY)) {
                    String profileKey = data.getStringExtra(Server.PROFILE_KEY);
                    if (!TextUtils.isEmpty(profileKey)) {
                        startVpnConnect(Server.VPN.getServerFromKey(profileKey));
                    }
                }
                break;
            default:
                initUI();
                break;
        }

    }

    private void saveProfiles(final VpnProfile vpnProfile) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                HashMap<String, VpnProfile> mapAllProfiles = system.getMapAllProfiles();
                mapAllProfiles.put(vpnProfile.mName, vpnProfile);
                system.setMapAllProfiles(mapAllProfiles);
            }
        });
    }

    private void showToastError(String textMessage ) {
        isToastPossibleToShow = false;
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_error,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView textView  = (TextView) layout.findViewById(R.id.textMessage);
        textView.setText(textMessage);

        final Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /**
     * Listener for clicks on FavoriteList
     */
    private class FavoriteListClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            Server.VPN server = favoriteAdapter.getItem(which);
            tvTitle.setText(getString(server.getTitle()));
            prefs.setLastServer(server);
            setBackgroundForCurrentServer(coordinatorMain, MainActivity.this, server);
            dialog.dismiss();
            startVpnConnect(server);
        }
    }

    /**
     * Listener for clicks on AllVPNList
     */
    private class AllVPNClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, AllVPNsActivity.class);
            startActivityForResult(intent, Const.SELECT_SERVERS);
        }
    }

    @Override
    public void setBackgroundForCurrentServer(CoordinatorLayout coordinatorLayout, Context context,
                                              Server.VPN server) {
        Drawable background = BackgroundHelper.getBackground(this, server);
        coordinatorLayout.setBackgroundDrawable(background);
    }

    /**
     * EventBus methods.
     * @param event
     */
    @Subscribe
    public void eventVpnState(EventUpdateVpnState event) {
//        String state = event.getState();
//        final int eventResID = event.getLocalizedResId();
//        final VpnStatus.ConnectionStatus eventLevel = event.getLevel();
//        Log.i(Const.LOG, "MainActivity -> eventVpnState() -> state:" + state);
//        Log.i(Const.LOG, "MainActivity.class -> eventVpnState() ->  logMessage = " + event.getLogmessage());
//        Log.i(Const.LOG, "MainActivity.class -> eventVpnState() ->  level = " + eventLevel.toString());
//        Log.i(Const.LOG, "MainActivity.class -> eventVpnState() ->  localized message = " + getString(eventResID));
//        Log.i(Const.LOG, " ===== ===== ==== ===== ===== ==== ===== ===== ==== ===== ===== ====");

        //prepare Thread. Not start!
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                Log.e(Const.LOG, "execute force exit");
                isToastPossibleToShow = true;
                busProvider.post(new EventUpdateVpnState(Const.EXITING,
                        Const.EXECUTE_FORCE_EXIT, R.string.execute_force_exit,
                        VpnStatus.ConnectionStatus.LEVEL_AUTH_FAILED));
            }
        });


        if(Const.TLS_ERROR.equals(event.getLogmessage()) && isToastPossibleToShow && Const.RECONNECTING.equals(event.getState())
                && Const.LEVEL_CONNECTING_NO_SERVER_REPLY_YET.equals(event.getLevel().toString())){
            showToastError(getString(R.string.error_failed_connect));
            thread.start(); //start Thread
        }

        else if (Const.AUTH_FAILED.equals(event.getState()) && isToastPossibleToShow) {
            showToastError(getString(R.string.error_invalidate_password_username));
            thread.start();//start Thread
        }

        else if (Const.EXITING.equals(event.getState()) && Const.EXECUTE_FORCE_EXIT.equals(event.getLogmessage())) {
            disconnectVpn();
        }

        setConnectStatusAndThumb();
    }

    @Subscribe
    public void errorVpn(EventErrorVpn event) {
//        String error = event.getLogItem().getString(Application.getContext());
//        Log.e(Const.LOG, "MainActivity -> errorVpn() -> state:" + event.getLogItem().getString(system.getSystemContext()));
    }
}
