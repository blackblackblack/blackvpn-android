package com.blackvpn.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Helpers.BackgroundHelper;
import com.blackvpn.Utils.Prefs;

public class HelpActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    private WebView webView;
    private BlackVpnSystem system = BlackVpnSystem.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.clHelpActivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
        webView = (WebView) findViewById(R.id.webView);
        if (system.isNetworkConnected()) {
            webView.loadUrl(Const.FAQ_URL);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        coordinatorLayout.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));
    }

    // Handle click to the back arrow on the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
