package com.blackvpn.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;

import java.util.Locale;

public class BuyNowActivity extends AppCompatActivity {
    private TextView tvYourPackage;
    private TextView tvYourPrice;
    private TextView tvYourPackage_value;
    private TextView tvYourPrice_value;

    private TextView tvPrivacy_1_line;
    private TextView tvPrivacy_2_line;
    private TextView tvPrivacy_3_line;
    private TextView tvPrivacy_4_line;
    private ImageView ivPrivacy;

    private TextView tvUSA_1_line;
    private TextView tvUSA_2_line;
    private TextView tvUSA_3_line;
    private TextView tvUSA_4_line;
    private ImageView ivUSA;

    private TextView tvUK_1_line;
    private TextView tvUK_2_line;
    private TextView tvUK_3_line;
    private TextView tvUK_4_line;
    private ImageView ivUK;

    private TextView tvPrivacyCloudText;
    private ImageView ivPrivacyCloud;
    private TextView tvPrivacyFilmText;
    private ImageView ivPrivacyFilm;

    private TextView tvUSA_YoutubeText;
    private ImageView ivUSA_Youtube;
    private TextView tvUSA_WIFI_Text;
    private ImageView ivUSA_WIFI;

    private TextView tvUK_YoutubeText;
    private ImageView ivUK_Youtube;
    private TextView tvUK_Eye_Text;
    private ImageView ivUK_Eye;

    private TextView tvYourVPNs;
    private TextView tvYourVPNs_value;

    private RelativeLayout rlBtnPRIVACY;
    private RelativeLayout rlBtnUSA;
    private RelativeLayout rlBtnUK;

    private Button btnBuy;
    private enum ButtonBuyState {DEFAULT, PRIVACY, USA, UK, TV, GLOBAL}
    ButtonBuyState buttonBuyState;

    private boolean privacyIsChecked = false;
    private boolean usaIsChecked = false;
    private boolean ukIsChecked = false;

    BlackVpnSystem system = BlackVpnSystem.getInstance();
    Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_now);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
        prefs = system.getPrefs();

        setUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setUI() {
        tvYourPackage = (TextView) findViewById(R.id.tvYourPackage);
        tvYourPrice = (TextView) findViewById(R.id.tvYourPrice);
        tvYourPackage_value = (TextView) findViewById(R.id.tvYourPackage_value);
        tvYourPrice_value = (TextView) findViewById(R.id.tvYourPrice_value);

        tvPrivacy_1_line = (TextView) findViewById(R.id.tvPrivacy_1_line);
        tvPrivacy_2_line = (TextView) findViewById(R.id.tvPrivacy_2_line);
        tvPrivacy_3_line = (TextView) findViewById(R.id.tvPrivacy_3_line);
        tvPrivacy_4_line = (TextView) findViewById(R.id.tvPrivacy_4_line);
        ivPrivacy = (ImageView) findViewById(R.id.ivPrivacy);

        tvUSA_1_line = (TextView) findViewById(R.id.tvUSA_1_line);
        tvUSA_2_line = (TextView) findViewById(R.id.tvUSA_2_line);
        tvUSA_3_line = (TextView) findViewById(R.id.tvUSA_3_line);
        tvUSA_4_line = (TextView) findViewById(R.id.tvUSA_4_line);
        ivUSA = (ImageView) findViewById(R.id.ivUSA);

        tvUK_1_line = (TextView) findViewById(R.id.tvUK_1_line);
        tvUK_2_line = (TextView) findViewById(R.id.tvUK_2_line);
        tvUK_3_line = (TextView) findViewById(R.id.tvUK_3_line);
        tvUK_4_line = (TextView) findViewById(R.id.tvUK_4_line);
        ivUK = (ImageView) findViewById(R.id.ivUK);

        tvPrivacyCloudText = (TextView) findViewById(R.id.tvPrivacyCloudText);
        ivPrivacyCloud = (ImageView) findViewById(R.id.ivPrivacyCloud);
        tvPrivacyFilmText = (TextView) findViewById(R.id.tvPrivacyFilmText);
        ivPrivacyFilm = (ImageView) findViewById(R.id.ivPrivacyFilm);

        tvUSA_YoutubeText = (TextView) findViewById(R.id.tvUSA_YoutubeText);
        ivUSA_Youtube = (ImageView) findViewById(R.id.ivUSA_Youtube);
        tvUSA_WIFI_Text = (TextView) findViewById(R.id.tvUSA_WIFI_Text);
        ivUSA_WIFI = (ImageView) findViewById(R.id.ivUSA_WIFI);

        tvUK_YoutubeText = (TextView) findViewById(R.id.tvUK_YoutubeText);
        ivUK_Youtube = (ImageView) findViewById(R.id.ivUK_Youtube);
        tvUK_Eye_Text = (TextView) findViewById(R.id.tvUK_Eye_Text);
        ivUK_Eye = (ImageView) findViewById(R.id.ivUK_Eye);

        tvYourVPNs = (TextView) findViewById(R.id.tvYourVPNs);
        tvYourVPNs_value = (TextView) findViewById(R.id.tvYourVPNs_value);

        rlBtnPRIVACY = (RelativeLayout) findViewById(R.id.rlBtnPRIVACY);
        rlBtnUSA = (RelativeLayout) findViewById(R.id.rlBtnUSA);
        rlBtnUK = (RelativeLayout) findViewById(R.id.rlBtnUK);

        rlBtnPRIVACY.setOnClickListener(new Button_Privacy_ClickListener());
        rlBtnUSA.setOnClickListener(new Button_USA_ClickListener());
        rlBtnUK.setOnClickListener(new Button_UK_ClickListener());

        btnBuy = (Button) findViewById(R.id.btnBuy);
        btnBuy.setOnClickListener(new Button_BUY_ClickListener());
        buttonBuyState = ButtonBuyState.DEFAULT;


//        privacyIsChecked = false;
//        usaIsChecked = false;
//        ukIsChecked = false;
//
//        setDefaultValue();

        privacyIsChecked = true;
        usaIsChecked = true;
        ukIsChecked = true;
        setGlobalPackage(Const.ALL);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initUI();
    }

    private class Button_Privacy_ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            privacyIsChecked = !privacyIsChecked;
            initUI();
        }
    }


    private class Button_USA_ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            usaIsChecked = !usaIsChecked;
            initUI();
        }
    }


    private class Button_UK_ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            ukIsChecked = !ukIsChecked;
            initUI();
        }
    }

    private CharSequence createTextForPrice(int sumInMonth, int sumInYear) {
        StringBuilder builder = new StringBuilder();
        builder.append("");
        builder.append(getResources().getString(sumInMonth));
        builder.append("/");
        builder.append(getResources().getString(R.string.month));
        builder.append("\n");
        builder.append(getResources().getString(sumInYear));
        builder.append("/");
        builder.append(getResources().getString(R.string.year));

        return builder.toString();
    }

    private void checkLocale() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        android.content.res.Configuration conf = getResources().getConfiguration();
        conf.locale = Locale.getDefault();
        getResources().updateConfiguration(conf, displayMetrics);
    }

    private void initUI() {

        checkLocale();

        //false false false
        if (!privacyIsChecked && !usaIsChecked && !ukIsChecked) {
            setDefaultValue();
        }

        //true false false
        if (privacyIsChecked && !usaIsChecked && !ukIsChecked) {
            setPrivacyPackage();
        }

        //true true false
        if (privacyIsChecked && usaIsChecked && !ukIsChecked) {
            setGlobalPackage(Const.USA);
        }

        //true true true
        if (privacyIsChecked && usaIsChecked && ukIsChecked) {
            setGlobalPackage(Const.ALL);
        }

        //true false true
        if (privacyIsChecked && !usaIsChecked && ukIsChecked) {
            setGlobalPackage(Const.UK);
        }

        //false true false
        if (!privacyIsChecked && usaIsChecked && !ukIsChecked) {
            set_USA_Package();
        }

        //false false true
        if (!privacyIsChecked && !usaIsChecked && ukIsChecked) {
            set_UK_Package();
        }

        //false true true
        if (!privacyIsChecked && usaIsChecked && ukIsChecked) {
            set_TV_Package();
        }
    }


    private void setDefaultValue() {
        //false false false
        ivPrivacy.setImageResource(R.drawable.buy_off);
        ivUSA.setImageResource(R.drawable.buy_off);
        ivUK.setImageResource(R.drawable.buy_off);

        ivPrivacyCloud.setImageResource(R.drawable.cloud_off);
        ivPrivacyFilm.setImageResource(R.drawable.film_off);
        ivUSA_Youtube.setImageResource(R.drawable.youtube_off);
        ivUSA_WIFI.setImageResource(R.drawable.wifi_off);
        ivUK_Youtube.setImageResource(R.drawable.youtube_off);
        ivUK_Eye.setImageResource(R.drawable.eye_off);

        tvPrivacy_1_line.setVisibility(View.VISIBLE);
        tvPrivacy_4_line.setVisibility(View.VISIBLE);
        tvPrivacy_1_line.setText(R.string.add_our);
        tvPrivacy_2_line.setText(R.string.privacy_vpns);
        tvPrivacy_3_line.setText(R.string.for_only);
        tvPrivacy_4_line.setText(R.string.euro_5_00);

        tvUSA_1_line.setVisibility(View.VISIBLE);
        tvUSA_4_line.setVisibility(View.VISIBLE);
        tvUSA_1_line.setText(R.string.add_our);
        tvUSA_2_line.setText(R.string.usa_vpns);
        tvUSA_3_line.setText(R.string.for_only);
        tvUSA_4_line.setText(R.string.euro_5_00);

        tvUK_1_line.setVisibility(View.VISIBLE);
        tvUK_4_line.setVisibility(View.VISIBLE);
        tvUK_1_line.setText(R.string.add_our);
        tvUK_2_line.setText(R.string.uk_vpns);
        tvUK_3_line.setText(R.string.for_only);
        tvUK_4_line.setText(R.string.euro_5_00);

        tvYourPackage_value.setText(getResources().getString(R.string.none));
        tvYourPrice_value.setText(getResources().getString(R.string.zero));
        tvYourVPNs_value.setText(R.string.none);

        buttonBuyState = ButtonBuyState.DEFAULT;
    }

    private void setPrivacyPackage() {
        //true false false
        ivPrivacy.setImageResource(R.drawable.buy_on);
        ivUSA.setImageResource(R.drawable.buy_off);
        ivUK.setImageResource(R.drawable.buy_off);

        ivPrivacyCloud.setImageResource(R.drawable.cloud_on);
        ivPrivacyFilm.setImageResource(R.drawable.film_on);
        ivUSA_Youtube.setImageResource(R.drawable.youtube_off);
        ivUSA_WIFI.setImageResource(R.drawable.wifi_on);
        ivUK_Youtube.setImageResource(R.drawable.youtube_off);
        ivUK_Eye.setImageResource(R.drawable.eye_on);

        tvPrivacy_1_line.setVisibility(View.GONE);
        tvPrivacy_2_line.setText(R.string.remove_our);
        tvPrivacy_3_line.setText(getResources().getString(R.string.privacy_vpns) + "?");
        tvPrivacy_4_line.setVisibility(View.GONE);

        tvUSA_1_line.setVisibility(View.VISIBLE);
        tvUSA_4_line.setVisibility(View.VISIBLE);
        tvUSA_1_line.setText(R.string.add_our);
        tvUSA_2_line.setText(R.string.us_and_uk_vpns);
        tvUSA_3_line.setText(R.string.for_only);
        tvUSA_4_line.setText(R.string.euro_4_50);

        tvUK_1_line.setVisibility(View.VISIBLE);
        tvUK_4_line.setVisibility(View.VISIBLE);
        tvUK_1_line.setText(R.string.add_our);
        tvUK_2_line.setText(R.string.us_and_uk_vpns);
        tvUK_3_line.setText(R.string.for_only);
        tvUK_4_line.setText(R.string.euro_4_50);

        tvYourPackage_value.setText(R.string.privacy);
        tvYourPrice_value.setText(createTextForPrice(R.string.euro_5, R.string.euro_49));
        tvYourVPNs_value.setText(R.string.vpns_privacy_package);

        buttonBuyState = ButtonBuyState.PRIVACY;
    }

    private void setGlobalPackage(String value) {

        //invariable fields (неизменяемые поля)
        ivPrivacyCloud.setImageResource(R.drawable.cloud_on);
        ivPrivacyFilm.setImageResource(R.drawable.film_on);
        ivUSA_Youtube.setImageResource(R.drawable.youtube_on);
        ivUSA_WIFI.setImageResource(R.drawable.wifi_on);
        ivUK_Youtube.setImageResource(R.drawable.youtube_on);
        ivUK_Eye.setImageResource(R.drawable.eye_on);

        tvPrivacy_1_line.setVisibility(View.GONE);
        tvPrivacy_2_line.setText(R.string.remove_our);
        tvPrivacy_3_line.setText(getResources().getString(R.string.privacy_vpns) + "?");
        tvPrivacy_4_line.setVisibility(View.GONE);

        tvYourPackage_value.setText(R.string.global);
        tvYourPrice_value.setText(createTextForPrice(R.string.euro_9_50, R.string.euro_99));
        tvYourVPNs_value.setText(R.string.vpns_global_package);

        buttonBuyState = ButtonBuyState.GLOBAL;


        //variable
        if (Const.USA.equals(value)) { //true true false
            ivPrivacy.setImageResource(R.drawable.buy_on);
            ivUSA.setImageResource(R.drawable.buy_on);
            ivUK.setImageResource(R.drawable.buy_off);

            tvUSA_1_line.setVisibility(View.GONE);
            tvUSA_4_line.setVisibility(View.GONE);
            tvUSA_2_line.setText(R.string.remove_our);
            tvUSA_3_line.setText(getResources().getString(R.string.usa_vpns) + "?");

            tvUK_1_line.setVisibility(View.GONE);
            tvUK_4_line.setVisibility(View.GONE);
            tvUK_2_line.setText(R.string.you_get_the);
            tvUK_3_line.setText(R.string.uk_vpns_free);
        }

        if (Const.UK.equals(value)) { //true false true
            ivPrivacy.setImageResource(R.drawable.buy_on);
            ivUSA.setImageResource(R.drawable.buy_off);
            ivUK.setImageResource(R.drawable.buy_on);

            tvUSA_1_line.setVisibility(View.GONE);
            tvUSA_4_line.setVisibility(View.GONE);
            tvUSA_2_line.setText(R.string.you_get_the);
            tvUSA_3_line.setText(R.string.usa_vpns_free);

            tvUK_1_line.setVisibility(View.GONE);
            tvUK_4_line.setVisibility(View.GONE);
            tvUK_2_line.setText(R.string.remove_our);
            tvUK_3_line.setText(getResources().getString(R.string.uk_vpns) + "?");
        }

        if (Const.ALL.equals(value)) { //true true true
            ivPrivacy.setImageResource(R.drawable.buy_on);
            ivUSA.setImageResource(R.drawable.buy_on);
            ivUK.setImageResource(R.drawable.buy_on);

//            tvUSA_1_line.setTextColor(getResources().getColor(R.color.colorPrimaryText));
//            _1_line.setTextColor(getResources().getColor(R.color.colorPrimaryText));
//            tvUSA_1_line.setTextColor(getResources().getColor(R.color.colorPrimaryText));

            tvUSA_1_line.setVisibility(View.GONE);
            tvUSA_4_line.setVisibility(View.GONE);
            tvUSA_2_line.setText(R.string.remove_our);
            tvUSA_3_line.setText(getResources().getString(R.string.usa_vpns) + "?");

            tvUK_1_line.setVisibility(View.GONE);
            tvUK_4_line.setVisibility(View.GONE);
            tvUK_2_line.setText(R.string.remove_our);
            tvUK_3_line.setText(getResources().getString(R.string.uk_vpns) + "?");
        }


    }


    private void set_USA_Package() {
        //false true false
        ivPrivacy.setImageResource(R.drawable.buy_off);
        ivUSA.setImageResource(R.drawable.buy_on);
        ivUK.setImageResource(R.drawable.buy_off);

        tvPrivacy_1_line.setVisibility(View.VISIBLE);
        tvPrivacy_4_line.setVisibility(View.VISIBLE);
        tvPrivacy_1_line.setText(R.string.add_our);
        tvPrivacy_2_line.setText(R.string.privacy_vpns);
        tvPrivacy_3_line.setText(R.string.for_only);
        tvPrivacy_4_line.setText(R.string.euro_4_50);

        tvUSA_1_line.setVisibility(View.GONE);
        tvUSA_4_line.setVisibility(View.GONE);
        tvUSA_2_line.setText(R.string.remove_our);
        tvUSA_3_line.setText(getResources().getString(R.string.usa_vpns) + "?");

        tvUK_1_line.setVisibility(View.VISIBLE);
        tvUK_4_line.setVisibility(View.VISIBLE);
        tvUK_1_line.setText(R.string.add_our);
        tvUK_2_line.setText(R.string.uk_vpns);
        tvUK_3_line.setText(R.string.for_only);
        tvUK_4_line.setText(R.string.euro_2_50);

        ivPrivacyCloud.setImageResource(R.drawable.cloud_off);
        ivPrivacyFilm.setImageResource(R.drawable.film_on);
        ivUSA_Youtube.setImageResource(R.drawable.youtube_on);
        ivUSA_WIFI.setImageResource(R.drawable.wifi_on);
        ivUK_Youtube.setImageResource(R.drawable.youtube_off);
        ivUK_Eye.setImageResource(R.drawable.eye_on);

        tvYourPackage_value.setText(R.string.usa);
        tvYourPrice_value.setText(createTextForPrice(R.string.euro_5, R.string.euro_49));
        tvYourVPNs_value.setText(R.string.usa);

        buttonBuyState = ButtonBuyState.USA;
    }


    private void set_UK_Package() {
        //false false true
        ivPrivacy.setImageResource(R.drawable.buy_off);
        ivUSA.setImageResource(R.drawable.buy_off);
        ivUK.setImageResource(R.drawable.buy_on);

        tvPrivacy_1_line.setVisibility(View.VISIBLE);
        tvPrivacy_4_line.setVisibility(View.VISIBLE);
        tvPrivacy_1_line.setText(R.string.add_our);
        tvPrivacy_2_line.setText(R.string.privacy_vpns);
        tvPrivacy_3_line.setText(R.string.for_only);
        tvPrivacy_4_line.setText(R.string.euro_4_50);

        tvUSA_1_line.setVisibility(View.VISIBLE);
        tvUSA_4_line.setVisibility(View.VISIBLE);
        tvUSA_1_line.setText(R.string.add_our);
        tvUSA_2_line.setText(R.string.usa_vpns);
        tvUSA_3_line.setText(R.string.for_only);
        tvUSA_4_line.setText(R.string.euro_2_50);

        tvUK_1_line.setVisibility(View.GONE);
        tvUK_4_line.setVisibility(View.GONE);
        tvUK_2_line.setText(R.string.remove_our);
        tvUK_3_line.setText(getResources().getString(R.string.uk_vpns) + "?");

        ivPrivacyCloud.setImageResource(R.drawable.cloud_off);
        ivPrivacyFilm.setImageResource(R.drawable.film_on);
        ivUSA_Youtube.setImageResource(R.drawable.youtube_off);
        ivUSA_WIFI.setImageResource(R.drawable.wifi_on);
        ivUK_Youtube.setImageResource(R.drawable.youtube_on);
        ivUK_Eye.setImageResource(R.drawable.eye_on);

        tvYourPackage_value.setText(R.string.uk);
        tvYourPrice_value.setText(createTextForPrice(R.string.euro_5, R.string.euro_49));
        tvYourVPNs_value.setText(R.string.uk);

        buttonBuyState = ButtonBuyState.UK;
    }


    private void set_TV_Package() {
        //false true true
        ivPrivacy.setImageResource(R.drawable.buy_off);
        ivUSA.setImageResource(R.drawable.buy_on);
        ivUK.setImageResource(R.drawable.buy_on);

        tvPrivacy_1_line.setVisibility(View.VISIBLE);
        tvPrivacy_4_line.setVisibility(View.VISIBLE);
        tvPrivacy_1_line.setText(R.string.add_our);
        tvPrivacy_2_line.setText(R.string.privacy_vpns);
        tvPrivacy_3_line.setText(R.string.for_only);
        tvPrivacy_4_line.setText(R.string.euro_2_00);

        tvUSA_1_line.setVisibility(View.GONE);
        tvUSA_4_line.setVisibility(View.GONE);
        tvUSA_2_line.setText(R.string.remove_our);
        tvUSA_3_line.setText(getResources().getString(R.string.usa_vpns) + "?");

        tvUK_1_line.setVisibility(View.GONE);
        tvUK_4_line.setVisibility(View.GONE);
        tvUK_2_line.setText(R.string.remove_our);
        tvUK_3_line.setText(getResources().getString(R.string.uk_vpns) + "?");

        ivPrivacyCloud.setImageResource(R.drawable.cloud_off);
        ivPrivacyFilm.setImageResource(R.drawable.film_on);
        ivUSA_Youtube.setImageResource(R.drawable.youtube_on);
        ivUSA_WIFI.setImageResource(R.drawable.wifi_on);
        ivUK_Youtube.setImageResource(R.drawable.youtube_on);
        ivUK_Eye.setImageResource(R.drawable.eye_on);

        tvYourPackage_value.setText(R.string.tv);
        tvYourPrice_value.setText(createTextForPrice(R.string.euro_7_50, R.string.euro_75));
        tvYourVPNs_value.setText(R.string.usa_and_uk);

        buttonBuyState = ButtonBuyState.TV;
    }

    private class Button_BUY_ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String email = "";
            if (!TextUtils.isEmpty(prefs.getEmail())) {
                email = "?email=" + prefs.getEmail();
                email = email.replace("@", "%40");
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            switch (buttonBuyState) {
                case DEFAULT: return;
                case PRIVACY:
                    intent.setData(Uri.parse(Const.SHOP_BUY_PRIVACY + email));
                    break;
                case USA:
                    intent.setData(Uri.parse(Const.SHOP_BUY_USA + email));
                    break;
                case UK:
                    intent.setData(Uri.parse(Const.SHOP_BUY_UK + email));
                    break;
                case TV:
                    intent.setData(Uri.parse(Const.SHOP_BUY_TV + email));
                    break;
                case GLOBAL:
                    intent.setData(Uri.parse(Const.SHOP_BUY_GLOBAL + email));
                    break;
                default: return;
            }


            if (system.isNetworkConnected()) {
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(Const.LOG, "BuyNowActivity - error startActivity");
                }
            } else {
                Toast.makeText(BuyNowActivity.this, R.string.no_internet, Toast.LENGTH_LONG).show();
            }
        }
    }
}
