package com.blackvpn.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ScrollView;

import com.blackvpn.Adapters.AutoConnectSpinnerAdapter;
import com.blackvpn.Adapters.AutoConnectWiFiAdapter;
import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Prefs;
import com.blackvpn.Utils.Server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

/**
 * Contains different settings that allow users to modify  features and behaviors of the App.
 *
 * @author Andrew.Gahov@gmail.com  (08.05.17)
 */

public class AutoConnectActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.autoConnectSpinner)
    AppCompatSpinner spinner;
    @BindView(R.id.autoConnectRecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.connectOnUnknownWiFi)
    SwitchCompat connectOnUnknownWiFi;
    @BindView(R.id.connectOnAppStartUp)
    SwitchCompat connectOnAppStartUp;
    @BindView(R.id.startAppOnDeviceStartUp)
    SwitchCompat startAppOnDeviceStartUp;

    private HashMap<String, Server.VPN> vpnHashMap = new HashMap<>(); // temp storage for allVPNs
    private Prefs mPrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autoconnect);
        ButterKnife.bind(this);
        mPrefs = BlackVpnSystem.getInstance().getPrefs();
        initUI();
    }

    private void initUI() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }

        // prepare layout manager to recyclerView
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        setVpnList(); // list of VPN locations

        setWiFiList(); // list of WiFi networks

        setSwitches();
    }

    private void setSwitches() {
        connectOnUnknownWiFi.setChecked(mPrefs.isConnectOnUnknownWiFi());
        connectOnAppStartUp.setChecked(mPrefs.isConnectOnAppStartUp());
        startAppOnDeviceStartUp.setChecked(mPrefs.isStartAppOnDeviceStartUp());
    }

    @OnCheckedChanged(R.id.connectOnUnknownWiFi)
    public void onUnknownWiFiChanged(boolean checked) {
        if (mPrefs == null) mPrefs = BlackVpnSystem.getInstance().getPrefs();
        mPrefs.setConnectOnUnknownWiFi(checked);
    }

    @OnCheckedChanged(R.id.connectOnAppStartUp)
    public void onOnAppStartUpChanged(boolean checked) {
        if (mPrefs == null) mPrefs = BlackVpnSystem.getInstance().getPrefs();
        mPrefs.setConnectOnAppStartUp(checked);
    }

    @OnCheckedChanged(R.id.startAppOnDeviceStartUp)
    public void onStartAppOnDeviceStartUpChanged(boolean checked) {
        if (mPrefs == null) mPrefs = BlackVpnSystem.getInstance().getPrefs();
        mPrefs.setStartAppOnDeviceStartUp(checked);
    }




    private void setVpnList() {
        Server.VPN selectedServer;
        if (mPrefs.getAutoConnectVpnLocation() != null) {
            selectedServer = mPrefs.getAutoConnectVpnLocation();
        } else {
            selectedServer = Server.getDefaultServer();
        }

        ArrayList<String> allVpns = new ArrayList<>();
        for (Server.VPN vpn : Server.getSortedList(getResources())) {
            allVpns.add(getString(vpn.getTitle()));
            vpnHashMap.put(getString(vpn.getTitle()), vpn);
        }

        final AutoConnectSpinnerAdapter adapter = new AutoConnectSpinnerAdapter(this, allVpns,
                R.layout.autoconnect_spinner_normal_item, R.layout.autoconnect_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition(getString(selectedServer.getTitle())));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Server.VPN selectedVpn = vpnHashMap.get(adapter.getItem(position));
                if (mPrefs == null) {
                    mPrefs = BlackVpnSystem.getInstance().getPrefs();
                }
                mPrefs.setAutoConnectVpnLocation(selectedVpn);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // nothing for now
            }
        });

    }

    private void setWiFiList() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().
                getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> wifiConfigurationList = wifiManager.getConfiguredNetworks();

        // the first step we need save current configuration
        BlackVpnSystem.getInstance().saveWiFiConfigurationList(wifiConfigurationList);

        // and only after it we make new adapter
        AutoConnectWiFiAdapter adapter = new AutoConnectWiFiAdapter(wifiConfigurationList);
        recyclerView.setAdapter(adapter);
    }

    // Handle click to the back arrow on the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto start activity.
                goToStartActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        goToStartActivity();
    }

    private void goToStartActivity() {
        Intent splashIntent = new Intent(AutoConnectActivity.this, Splash.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(splashIntent);
        this.finish();
    }
}
