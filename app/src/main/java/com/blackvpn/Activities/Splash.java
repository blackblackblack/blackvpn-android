package com.blackvpn.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blackvpn.Events.BusProvider;
import com.blackvpn.Events.EventMessage;
import com.blackvpn.R;
import com.blackvpn.RegistrationIntentService;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Helpers.BackgroundHelper;
import com.blackvpn.Utils.Helpers.GcmMessages;
import com.blackvpn.Utils.Prefs;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Splash extends AppCompatActivity {

    private static final int GCM_RESOLUTION_REQUEST = 11000;

    private final int WAIT_TIME = 600;
    private BlackVpnSystem system;
    private EditText etEmailAddress;
    private Button btnSave;
    private RelativeLayout rlNeedEmail;
    private RelativeLayout splash_content;
    private boolean isOpened = false;
    private String token;
    private static Handler handler;
    private ImageView ivBackground;
    private Button btnSkipTrial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        system = BlackVpnSystem.getInstance();

        initUI();

        handler = new Handler();

        if (system.getPrefs().getGcmToken() == null) {
            startService(new Intent(this, RegistrationIntentService.class));
        }

    }


    private void initUI() {
        etEmailAddress = (EditText) findViewById(R.id.input_email_address);
        rlNeedEmail = (RelativeLayout) findViewById(R.id.rlNeedEmail);
        splash_content = (RelativeLayout) findViewById(R.id.splash_content);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if (isEmailValidate()) {
                    system.getPrefs().setEmail(etEmailAddress.getText().toString());
                    setState();
                }
            }
        });
        ivBackground = (ImageView) findViewById(R.id.ivBackground);
        ivBackground.setImageDrawable(BackgroundHelper.getBackground(this, null));
        btnSkipTrial = (Button) findViewById(R.id.btnSkipTrial);
        btnSkipTrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAccountActivity();
            }
        });
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setState();
    }

    private void setState() {
        if (weHaveEmail() && weHaveLoginAndPass()) {
            rlNeedEmail.setVisibility(View.INVISIBLE);
            splash_content.setVisibility(View.VISIBLE);
            openMainActivity();
        } else if (!weHaveEmail() && !weHaveLoginAndPass()){
            ivBackground.setImageDrawable(BackgroundHelper.getBackground(this, null));
            rlNeedEmail.setVisibility(View.VISIBLE);
            splash_content.setVisibility(View.INVISIBLE);
        } else if (weHaveEmail() && !weHaveLoginAndPass()) {
            ivBackground.setImageDrawable(BackgroundHelper.getBackground(this, null));
            rlNeedEmail.setVisibility(View.INVISIBLE);
            splash_content.setVisibility(View.VISIBLE);

            requestToServer();
        }
    }

    private void requestToServer() {
        Log.d(Const.LOG, "Splash.class -> requestToServer() ");
        try {
            BusProvider.getInstance().register(this);
        } catch (IllegalArgumentException e) {
            //Object already registered
//            e.printStackTrace();
        }
        if (system.isNetworkConnected()) {

            String stringIdentifier = android.os.Build.SERIAL;
            if (stringIdentifier == null) {
                Settings.Secure.getString(getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            }

            String email = system.getPrefs().getEmail();
            new ServerRequest(token, email, stringIdentifier).execute((Void) null);
        } else {
            BusProvider.getInstance().post(new EventMessage(Const.GCM_ERROR));
        }
    }

    private boolean weHaveLoginAndPass() {
        if (TextUtils.isEmpty(system.getPrefs().getPassword())
                || TextUtils.isEmpty(system.getPrefs().getUserName())) {
            return false;
        } else return true;
    }

    private void openAccountActivity() {
        Intent mainIntent = new Intent(Splash.this, AccountActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Splash.this.startActivity(mainIntent);
        Splash.this.finish();
    }

    private void openMainActivity() {
        if (!isOpened) {
            isOpened = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent mainIntent = new Intent(Splash.this, MainActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                }
            }, WAIT_TIME);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(Const.LOG, "Splash.class -> finish");
                }
            }, WAIT_TIME);
        }
    }

    private boolean weHaveEmail() {
        String email = system.getPrefs().getEmail();
        if (TextUtils.isEmpty(email)) return false;
        else return true;
    }




    // validation email
    private boolean isEmailValidate() {
        String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"-<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        String email = etEmailAddress.getText().toString();
        Matcher matcher = pattern.matcher(email);

        if (email != null && !email.isEmpty() && matcher.matches()) {
            return true;
        } else {
            etEmailAddress.setError(getString(R.string.error_validate_email));
            return false;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            BusProvider.getInstance().unregister(this);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Subscribe
    public void onEvent(EventMessage event) {

        if (Const.GCM_ERROR.equals(event.getMarker())) {
            Log.d(Const.LOG, "Splash.class -> GCM_ERROR");
            handler.removeCallbacksAndMessages(null);
            showErrorDialog();
        }
        if (Const.GCM_UPDATE_TOKEN.equals(event.getMarker())) {
            Log.d(Const.LOG, "Splash.class -> GCM_UPDATE_TOKEN");
            token = system.getPrefs().getGcmToken();
        }
        if (Const.GCM_RECEIVED_MESSAGE.equals(event.getMarker())) {
            Log.d(Const.LOG, "Splash.class -> GCM_RECEIVED_MESSAGE");
            handler.removeCallbacksAndMessages(null);
            String message = GcmMessages.getText(event.getMessage(), this);
            showWelcomeDialog(message);
        }
        if (Const.GCM_MESSAGE_WITH_ERROR.equals(event.getMarker())) {
            Log.d(Const.LOG, "Splash.class -> GCM_MESSAGE_WITH_ERROR");
            handler.removeCallbacksAndMessages(null);
            String message = GcmMessages.getText(event.getMessage(), this);
            showMessageErrorDialog(message);
        }
    }

    private void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.no_connection_to_the_server));
        builder.setPositiveButton(getResources().getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
//                        finish();
                        setState();
                    }
                }
        );
        builder.create().show();
    }

    private void showMessageErrorDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton(getResources().getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openAccountActivity();
                    }
                }
        );
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onResume();
            }
        });
        builder.create().show();
    }

    private void showWelcomeDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton(getResources().getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setState();
                    }
                }
        );
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                setState();
            }
        });
        builder.create().show();
    }


    private class ServerRequest extends AsyncTask<Void, Void, Void> {
        private String token;
        private String email;
        private String stringIdentifier;

        public ServerRequest(String token, String email, String stringIdentifier) {
            this.token = token;
            this.email = email;
            this.stringIdentifier = stringIdentifier;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());

            String senderId = getString(R.string.gcm_defaultSenderId);

            // Create the bundle for sending the message.
            Bundle message = new Bundle();
            message.putString("action", "upstream_message");
            message.putString("message", email);
            message.putString("stringIdentifier", stringIdentifier);

            try {
                gcm.send(senderId + "@gcm.googleapis.com",
                        String.valueOf(System.currentTimeMillis()), message);
                Log.d(Const.LOG, "Splash.class -> gcm.send -> Message sent successfully");
            } catch (IOException e) {
                Log.e(Const.LOG, "Message failed", e);
                Log.d(Const.LOG, "Splash.class -> gcm.send -> Message Upstream FAILED");
                BusProvider.getInstance().post(new EventMessage(Const.GCM_ERROR));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    BusProvider.getInstance().post(new EventMessage(Const.GCM_ERROR));
                }
            }, GCM_RESOLUTION_REQUEST);
        }
    }

}
