package com.blackvpn.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackvpn.BuildConfig;
import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Helpers.BackgroundHelper;
import com.blackvpn.Utils.Prefs;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountActivity extends AppCompatActivity {
    private BlackVpnSystem system;
    private Toolbar toolbar;

    private TextView tvUserName;
    private TextView tvPassword;
    private SpannableString contentUser;
    private SpannableString contentPassword;

    private EditText etEmailAddress;
    private EditText etUserName;
    private EditText etPassword;
    private ImageView shoppingMenu;
    private RelativeLayout mDialog;
    private CoordinatorLayout clAccountCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        system = BlackVpnSystem.getInstance();

        setUI();

        initUI();
    }

    private void checkLocale() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        android.content.res.Configuration conf = getResources().getConfiguration();
        conf.locale = Locale.getDefault();
        getResources().updateConfiguration(conf, displayMetrics);
    }

    private void setUI() {
        checkLocale();

        etEmailAddress = (EditText) findViewById(R.id.input_email_address);
        etUserName = (EditText) findViewById(R.id.input_name);
        etPassword = (EditText) findViewById(R.id.input_password);
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    hidePasswordField(false);
                }
            }
        });

        clAccountCoordinator = (CoordinatorLayout) findViewById(R.id.clAccountActivity);
        tvUserName = (TextView) findViewById(R.id.text_view_lost_username);
        tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Const.LOST_USER_NAME_URL));
                startActivity(i);
            }
        });
        tvPassword = (TextView) findViewById(R.id.text_view_lost_password);
        tvPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(Const.LOST_USER_PASSWORD_URL));
                startActivity(i);
            }
        });

        contentUser = new SpannableString(getString(R.string.lost_you_vpn_user_name));
        contentUser.setSpan(new UnderlineSpan(), 0, contentUser.length(), 0);

        contentPassword = new SpannableString(getString(R.string.lost_you_vpn_password));
        contentPassword.setSpan(new UnderlineSpan(), 0, contentPassword.length(), 0);
        mDialog = (RelativeLayout) findViewById(R.id.shopMessenger);
        mDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AccountActivity.this, BuyNowActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
            }
        });
        setToolbar();

        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                if (isEmailValidate()) {
                    saveDateInPrefs();
                }
            }
        });

    }

    // Сохраняет все настройки в Prefs
    private void saveDateInPrefs() {
        system.getPrefs().setEmail(etEmailAddress.getText().toString());
        system.getPrefs().setPassword(etPassword.getText().toString());
        system.getPrefs().setUserName(etUserName.getText().toString());
        goToStartActivity();
    }

    // Метод который прячет клавиатуру
    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean isEmailValidate() {
        String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"-<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        String email = etEmailAddress.getText().toString();
        Matcher matcher = pattern.matcher(email);

        if (!email.isEmpty() && matcher.matches()) {
            return true;
        } else {
            etEmailAddress.setError(getString(R.string.error_validate_email));
            return false;
        }

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("email", etEmailAddress.getText().toString());
        outState.putString("pass", etPassword.getText().toString());
        outState.putString("login", etUserName.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        etEmailAddress.setText(savedInstanceState.getString("email"));
        etPassword.setText(savedInstanceState.getString("pass"));
        etUserName.setText(savedInstanceState.getString("login"));
    }

    private void initUI() {
        Prefs prefs = system.getPrefs();

        if (TextUtils.isEmpty(prefs.getEmail())
                || TextUtils.isEmpty(prefs.getUserName())
                || TextUtils.isEmpty(prefs.getPassword())) {

            etEmailAddress.setText(prefs.getEmail());
            etUserName.setText(prefs.getUserName());
            etPassword.setText(prefs.getPassword());

            if ((TextUtils.isEmpty(prefs.getPassword()))) {
                hidePasswordField(false);
            } else {
                hidePasswordField(true);
            }

//            showWarning();

        } else {

            etEmailAddress.setText(prefs.getEmail());
            etUserName.setText(prefs.getUserName());
            etPassword.setText(prefs.getPassword());
//            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            if (TextUtils.isEmpty(prefs.getPassword())) {
                hidePasswordField(false);
            } else {
                hidePasswordField(true);
            }

        }

        tvUserName.setText(contentUser);
        tvPassword.setText(contentPassword);

        clAccountCoordinator.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));
    }

    private void showWarning() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_error,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView textView = (TextView) layout.findViewById(R.id.textMessage);
        textView.setText(R.string.set_data);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void hidePasswordField(boolean isHide) {
        if (isHide) {
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            etPassword.setTransformationMethod(null);
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        }
    }

    // Handle click to the back arrow on the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto start activity.
                goToStartActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        goToStartActivity();
    }

    private void goToStartActivity() {
        Intent splashIntent = new Intent(AccountActivity.this, Splash.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(splashIntent);
        this.finish();
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.account_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_account));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
        shoppingMenu = (ImageView) toolbar.findViewById(R.id.shopping_cart);
        TextView title = (TextView) toolbar.findViewById(R.id.title_account);
        title.setText(getResources().getString(R.string.title_activity_account));
        shoppingMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AccountActivity.this, BuyNowActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
            }
        });
    }
}
