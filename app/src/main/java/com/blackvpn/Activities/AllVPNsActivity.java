package com.blackvpn.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.blackvpn.Adapters.AllVPNsAdapter;
import com.blackvpn.R;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Helpers.BackgroundHelper;

public class AllVPNsActivity extends AppCompatActivity {
    private CoordinatorLayout mCoordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allvpns);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.clAllVPNActivity);
        setUI();
    }

    private void setUI() {
        setToolbar();

        ListView lvServers = (ListView) findViewById(R.id.lvServers);
        AllVPNsAdapter adapter = new AllVPNsAdapter(this, BlackVpnSystem.getInstance().getPrefs(), getResources());
        lvServers.addHeaderView(getLayoutInflater().inflate(R.layout.header_for_listview_allvpns, null));
        lvServers.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mCoordinatorLayout.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
    }


    // Handle click to the back arrow on the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
        this.finish();
    }
}
