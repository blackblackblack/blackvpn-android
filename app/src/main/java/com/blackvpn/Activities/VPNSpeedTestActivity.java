package com.blackvpn.Activities;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.blackvpn.R;
import com.blackvpn.Utils.Helpers.BackgroundHelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class VPNSpeedTestActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private WebView wvSpeedTest;
    private CoordinatorLayout clTermsCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vpnspeed_test);
        setUI();
    }

    private void setUI() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setWebView();
        setToolbar();
    }

    private void setWebView() {
        wvSpeedTest = (WebView) findViewById(R.id.wvSpeedTest);
        wvSpeedTest.setBackgroundColor(0x00000000);
        wvSpeedTest.loadUrl("https://www.blackvpn.com/speedtest/android-app.html");

        wvSpeedTest.getSettings().setJavaScriptEnabled(true);
        wvSpeedTest.setBackgroundColor(0x00000000);

        try {
            Method method = View.class.getMethod("setLayerType", int.class, Paint.class);
            method.invoke(wvSpeedTest, 1, new Paint()); // 1 = LAYER_TYPE_SOFTWARE (API11)
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_speed_test));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        initUI();
    }

    private void initUI() {
        clTermsCoordinator = (CoordinatorLayout) findViewById(R.id.clSpeedActivity);
        clTermsCoordinator.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));

        wvSpeedTest.setWebViewClient(webViewClient);
    }
    WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            wvSpeedTest.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            wvSpeedTest.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    };
}
