package com.blackvpn.Activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.blackvpn.R;
import com.blackvpn.Utils.Helpers.BackgroundHelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TermsActivity extends AppCompatActivity {
    private CoordinatorLayout clTermsCoordinator;
    private WebView wvContent;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        setUI();

    }

    private void setUI() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setWebView();
        setToolbar();
    }

    private void setWebView() {
        wvContent = (WebView) findViewById(R.id.wvContent);
        wvContent.setBackgroundColor(0x00000000);
        wvContent.loadUrl("file:///android_asset/terms.html");

        wvContent.getSettings().setJavaScriptEnabled(true);
        wvContent.setBackgroundColor(0x00000000);

        try {
            Method method = View.class.getMethod("setLayerType", int.class, Paint.class);
            method.invoke(wvContent, 1, new Paint()); // 1 = LAYER_TYPE_SOFTWARE (API11)
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        initUI();
    }

    private void initUI() {
        clTermsCoordinator = (CoordinatorLayout) findViewById(R.id.clTermsActivity);
        clTermsCoordinator.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));

        wvContent.setWebViewClient(webViewClient);
    }

    // Handle click to the back arrow on the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            wvContent.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            wvContent.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    };
}
