package com.blackvpn.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.blackvpn.R;
import com.blackvpn.RestAPI.ApiSystem;
import com.blackvpn.RestAPI.ModelTicket;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Helpers.BackgroundHelper;
import com.blackvpn.Utils.Prefs;
import com.blackvpn.Utils.WriteLogs;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class SupportActivity extends AppCompatActivity {

    private BlackVpnSystem system;
    private CoordinatorLayout clSupportActivity;
    private Spinner mSpinner;
    private EditText mMessage;
    private ImageButton mButtonSubmitTicket;
    private Prefs mPrefs;
    private String[] dropDownStrings;
    private ArrayAdapter<String> mSpinnerArrayAdapter;
    private String mSelected = "";
    private CheckBox mAddFileDebug;
    private EditText mName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        system = BlackVpnSystem.getInstance();
        mPrefs = system.getPrefs();
        setToolbar();

        dropDownStrings = getResources().getStringArray(R.array.drop_down_strings);

        setUI();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true); //back arrow
        }
    }

    private void setUI() {
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mAddFileDebug = (CheckBox) findViewById(R.id.add_file_debug);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                mSelected = parent.getItemAtPosition(position).toString();
                mSelected = Const.DROP_DOWN_SPINNER_TICKET[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mMessage = (EditText) findViewById(R.id.input_message);
        mName = (EditText) findViewById(R.id.input_name);
        mButtonSubmitTicket = (ImageButton) findViewById(R.id.btnSubmitTicket);
        clSupportActivity = (CoordinatorLayout) findViewById(R.id.clSupportActivity);

        mButtonSubmitTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();

                ModelTicket ticket = getDateNewTicket();
                if (ticket != null) {
                    if (system.isNetworkConnected()) {
                        mButtonSubmitTicket.setEnabled(false);
                        Toast.makeText(
                                getApplicationContext(),
                                getString(R.string.trying_send),
                                Toast.LENGTH_LONG
                        ).show();

                        new RestAdapter.Builder()
                                .setEndpoint(Const.TICKET_URL)
                                .setClient(new OkClient(new OkHttpClient()))
                                .setLogLevel(RestAdapter.LogLevel.FULL)
                                .setConverter(new GsonConverter(
                                                new GsonBuilder()
                                                        .disableHtmlEscaping()
                                                        .create()
                                        )
                                )
                                .build()
                                .create(ApiSystem.TicketPost.class)
                                .createTicket(ticket, new Callback<String>() {
                                            @Override
                                            public void success(String s, Response response) {
                                                mButtonSubmitTicket.setEnabled(true);

                                                Toast.makeText(
                                                        getApplicationContext(),
//                                                        "Has been successfully sent, see the email",
//                                                        getString(R.string.ticket_successfully),
                                                        getString(R.string.ticket_successfully_see_email),
                                                        Toast.LENGTH_LONG
                                                ).show();
                                                finish();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                mButtonSubmitTicket.setEnabled(true);
                                                Toast.makeText(
                                                        getApplicationContext(),
                                                        getString(R.string.ticket_error),
                                                        Toast.LENGTH_LONG
                                                ).show();
                                            }
                                        }
                                );

                    } else {
                        Toast.makeText(
                                getApplicationContext(),
                                getString(R.string.no_internet),
                                Toast.LENGTH_LONG
                        ).show();
                    }

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        initUI();
    }

    private void initUI() {

        checkLocale();

        mSpinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dropDownStrings);
        mSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        mSpinner.setAdapter(mSpinnerArrayAdapter);

        clSupportActivity.setBackgroundDrawable(BackgroundHelper.getBackground(this, null));
    }

    private void checkLocale() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        android.content.res.Configuration conf = getResources().getConfiguration();
        conf.locale = Locale.getDefault();
        getResources().updateConfiguration(conf, displayMetrics);
    }

    // Handle click to the back arrow on the toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public ModelTicket getDateNewTicket() {
        String userName = mName.getText().toString();
        if (userName == null) {
            userName = "";
        }
        String email = mPrefs.getEmail();
        ModelTicket ticket = null;
        if (mMessage.getText() != null && !mMessage.getText().toString().isEmpty()) {
            ticket = new ModelTicket();
            if (email != null && !email.isEmpty()) {

                ticket.setAlert(true);
                ticket.setAutorespond(true);
                ticket.setSource("API");
                ticket.setName(userName);
                ticket.setEmail(email);
                ticket.setPhone("");
                ticket.setSubject(getSelectSpinner());
                ticket.setIp("0.0.0.0");
                ticket.setMessage(mMessage.getText().toString().replace("\n", " "));
                if (mAddFileDebug.isChecked()) {
                    ticket.setAttachmentUTF8(WriteLogs.readAllCachedText());
                }
            } else {
                Toast.makeText(
                        getApplicationContext(),
                        getString(R.string.email_empty),
                        Toast.LENGTH_LONG
                ).show();
                Intent intent = new Intent(this, AccountActivity.class);
                startActivity(intent);
                return null;
            }
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.error_validate_message), Toast.LENGTH_LONG).show();
        }

        return ticket;
    }

    private void closeKeyboard() {
        if (mMessage.isFocusable()) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mMessage.getWindowToken(), 0);

        }else {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mName.getWindowToken(), 0);

        }


    }

    public String getSelectSpinner() {
        if (!mSelected.isEmpty()) {
            return mSelected;
        } else {
            return Const.DROP_DOWN_SPINNER_TICKET[2];
        }
    }

}
