package com.blackvpn;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.blackvpn.Events.BusProvider;
import com.blackvpn.Events.EventMessage;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;
import com.google.android.gms.gcm.GcmListenerService;

public class GcmListener extends GcmListenerService {


    @Override
    public void onMessageReceived(String from, Bundle data) {
        final String message = data.getString("message");
        String password = data.getString("password");
        String username = data.getString("username");
        Log.d(Const.LOG, "GcmListener.class -> onMessageReceived() -> From: " + from);
        Log.d(Const.LOG, "GcmListener.class -> onMessageReceived() -> message: " + message);
        Log.d(Const.LOG, "GcmListener.class -> onMessageReceived() -> password: " + password);
        Log.d(Const.LOG, "GcmListener.class -> onMessageReceived() -> username: " + username);

        if (!TextUtils.isEmpty(message) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(username)) {
            Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
            prefs.setPassword(password);
            prefs.setUserName(username);
            Log.d(Const.LOG, "GcmListener.class -> we have new message (normal) - " + message);
            BusProvider.getInstance().post(new EventMessage(Const.GCM_RECEIVED_MESSAGE, message));
        }
        else if (!TextUtils.isEmpty(message)) {
            Log.d(Const.LOG, "GcmListener.class -> we have new message (error) - " + message);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    BusProvider.getInstance().post(new EventMessage(Const.GCM_MESSAGE_WITH_ERROR, message));
                }
            }, 1500);
        }
    }


    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
        Log.d(Const.LOG, "GcmListener.class -> onDeletedMessages()");
    }

    @Override
    public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
        Log.d(Const.LOG, "GcmListener.class -> onMessageSent() + msgId = " + msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        super.onSendError(msgId, error);
        Log.d(Const.LOG, "GcmListener.class -> onSendError() + msgId = " + msgId + ", error = " + error);
        BusProvider.getInstance().post(new EventMessage(Const.GCM_ERROR));
    }
}
