package com.blackvpn.Events;

import android.util.Log;

import de.blinkt.openvpn.core.VpnStatus;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;

public class VpnEvent implements
        VpnStatus.StateListener, VpnStatus.LogListener, VpnStatus.ByteCountListener  {
    private BlackVpnSystem system;
    private BusProvider busProvider;


    /**
     * в конструкторе задаем систему, подключаем ЭвентБас
     * и устанавливаем этот класс в качестве слушателей событий
     */
    public VpnEvent() {
        system = BlackVpnSystem.getInstance();
        busProvider = BusProvider.getInstance();

        //Добавляем класс как слушателя событий
//        Log.i(Const.LOG, "VpnEvent.class -> registerListener");
        VpnStatus.addStateListener(this);
        VpnStatus.addLogListener(this);
        VpnStatus.addByteCountListener(this);
    }

    public void removeListeners() {
        VpnStatus.removeByteCountListener(this);
        VpnStatus.removeLogListener(this);
        VpnStatus.removeStateListener(this);
    }


    /**
     * There is a counter of bytes that are going through VPN service.
     *
     * @param in      - bytes in
     * @param out     - bytes out
     * @param diffIn  - different from bytes in
     * @param diffOut - different from bytes out
     */
    @Override
    public void updateByteCount(long in, long out, long diffIn, long diffOut) {
//        Log.i(Const.LOG, "VpnEvent.class -> updateByteCount() -> "
//                        + "long in: " + in
//                        + ", long out: " + out
//                        + ", long diffIn: " + diffIn
//                        + ", long diffOut: " + diffOut
//        );
    }


    /**
     * There is a log to write of information to a file or a table.
     *
     * @param logItem - events
     */
    @Override
    public void newLog(VpnStatus.LogItem logItem) {

		// if LogLevel == -2 this is Error
        // В случае ошибки - создаем оповещение

        if (logItem.getLogLevel().getInt() == -2) {
//            Log.e(Const.LOG, "VpnEvent.class -> newLog() -> ERROR! -> "
//                    + String.valueOf(logItem.getLogLevel().getInt()) + ", logItem: "
//                    + logItem.getString(system.getSystemContext()));

            //сообщаем об ошибке
//            Log.i(Const.LOG, "VpnEvent.class -> busProvider: new EventErrorVpn()... event bus of error!");
            busProvider.post(new EventErrorVpn(logItem));

        } else {
            busProvider.post(new EventErrorVpn(logItem));
            //если ошибок нет - выводим обычный лог
//            Log.i(Const.LOG, "VpnEvent.class -> newLog() -> logItem -> "
//                    + logItem.getString(system.getSystemContext()));
        }
        busProvider.post(new AllEventVPN(logItem));
    }

    /**
     * Listener of state change.
     *
     * @param state          - state
     * @param logmessage     - message
     * @param localizedResId - localized message (id)
     * @param level          - level
     */
    @Override
    public void updateState(String state, String logmessage, int localizedResId,
                            VpnStatus.ConnectionStatus level) {

//        Log.i(Const.LOG, "VpnEvent.class -> updateState() ->  state = " + state);
//        Log.i(Const.LOG, "VpnEvent.class -> updateState() -> localizedState = "
//                + system.getSystemContext().getString(VpnStatus.getLocalizedState(state)));
//        Log.i(Const.LOG, "VpnEvent.class -> updateState() ->  logMessage = " + logmessage);
//        Log.i(Const.LOG, "VpnEvent.class -> updateState() ->  level = " + level.toString());

        if (!system.isBlocked()) {
            savePrefs(state);
        } else {
//            Log.e(Const.LOG, "VpnEvent.class -> savePrefs() -> don't save status. May be \"isBlocked\"==true");
        }

        busProvider.post(new EventUpdateVpnState(state, logmessage, localizedResId, level));
    }

    private void savePrefs(String state) {
        try {
            system.getPrefs().setStatusVPN(state);
        } catch (Exception e) {
            Log.e(Const.LOG, "VpnEvent.class -> savePrefs() -> May be no context for Prefs ");
        }
    }
}
