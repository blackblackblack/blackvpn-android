package com.blackvpn.Events;

import de.blinkt.openvpn.core.VpnStatus;

public class EventUpdateVpnState {
    private String state;
    private String logMessage;
    private int localizedResId;
    private VpnStatus.ConnectionStatus level;

    public EventUpdateVpnState(String state, String logMessage, int localizedResId, VpnStatus.ConnectionStatus level) {
        this.state = state;
        this.logMessage = logMessage;
        this.localizedResId = localizedResId;
        this.level = level;
    }

    public String getState() {
        return state;
    }

    public String getLogmessage() {
        return logMessage;
    }

    public int getLocalizedResId() {
        return localizedResId;
    }

    public VpnStatus.ConnectionStatus getLevel() {
        return level;
    }
}
