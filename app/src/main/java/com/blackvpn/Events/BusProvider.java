package com.blackvpn.Events;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.blackvpn.Utils.Const;
import com.squareup.otto.Bus;

public class BusProvider extends Bus {
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private static BusProvider busProvider = null;

    public static BusProvider getInstance(){
        if (busProvider == null) {
            busProvider = new BusProvider();
        }
        return busProvider;
    }

    @Override
    public void post(final Object event) {
        Log.d(Const.LOG, "BusProvider.class -> post() -> event -> " + event.getClass().getSimpleName());
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    BusProvider.super.post(event);
                }
            });
        }
    }
}