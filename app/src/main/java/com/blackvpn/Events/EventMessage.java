package com.blackvpn.Events;

public class EventMessage {
    private String marker;
    private String message;

    public EventMessage(String marker) {
        this.marker = marker;
    }

    public EventMessage(String marker, String message) {
        this.marker = marker;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getMarker() {
        return marker;
    }
}
