package com.blackvpn.Events;

import de.blinkt.openvpn.core.VpnStatus;

public class EventErrorVpn {
    private VpnStatus.LogItem logItem;

    EventErrorVpn(VpnStatus.LogItem logItem) {
        this.logItem = logItem;
    }

    public VpnStatus.LogItem getLogItem() {
        return logItem;
    }
}
