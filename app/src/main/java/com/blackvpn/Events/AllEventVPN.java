package com.blackvpn.Events;

import de.blinkt.openvpn.core.VpnStatus;

public class AllEventVPN {
    private VpnStatus.LogItem logItem;

    public AllEventVPN(VpnStatus.LogItem logItem) {
        this.logItem = logItem;
    }

    public VpnStatus.LogItem getLogItem() {
        return logItem;
    }
}
