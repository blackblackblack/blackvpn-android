package com.blackvpn.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.blackvpn.Activities.Splash;
import com.blackvpn.Application;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;

import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VpnStatus;

/**
 * Start app on device startup
 *
 * @author Andrew.Gahov@gmail.com  (11.05.17)
 */

public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Application.getContext() == null) {
            Log.e(Const.LOG, "BootCompletedReceiver has no context!");
            return;
        }

        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();

        disconnectVpn(context, prefs);

        Log.d(Const.LOG, "BootCompletedReceiver is started");

        if (prefs.isStartAppOnDeviceStartUp()) {
            intent = new Intent(context, Splash.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    /**
     * will disconnect for a VPN services
     * @param context Context
     * @param prefs Prefs
     */
    private void disconnectVpn(Context context, Prefs prefs) {
        prefs.setStatusVPN(Const.VPN_STATE_NO_PROCESS);
        ProfileManager.setConntectedVpnProfileDisconnected(context);
        VpnStatus.clearLog();
    }
}
