package com.blackvpn.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.blackvpn.Activities.Splash;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;

import java.util.ArrayList;
import java.util.List;

/**
 * It will be called when some WiFi network will be connected to the device
 *
 * @author Andrew.Gahov@gmail.com  (11.05.17)
 */

public class ChangeNetworkReceiver extends BroadcastReceiver {
    private static SupplicantState lastState = SupplicantState.UNINITIALIZED;
    private static int lastWifi = -1;

    @Override
    public void onReceive(Context context, Intent intent) {

        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);

        // check only WIFi
        if (wifiManager.isWifiEnabled()) {

            // The start.

            WifiInfo info = wifiManager.getConnectionInfo();

            SupplicantState currentState = info.getSupplicantState();
            if (lastState == currentState && info.getSSID().hashCode() == lastWifi) {
                return; // because it is a repeated call
            } else {
                lastState = currentState;
                lastWifi = info.getSSID().hashCode();
            }

            if (SupplicantState.COMPLETED != lastState) {
                return; // we need COMPLETED state only
            }

            String ssid = info.getSSID();

            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            if (list == null) {
                Log.e(Const.LOG, "ChangeNetworkReceiver -> disconnected ");
                return;
            }

            // Check WiFi configuration
            WifiConfiguration wifi = null;
            for (WifiConfiguration configuration : list) {
                if (configuration == null || configuration.SSID == null || ssid == null) continue;
                if (configuration.SSID.equals(ssid)) {
                    wifi = configuration;
                }
            }
            if (wifi == null || wifi.SSID == null) {
                return; // wifi not found
            }

            String wifiKey = BlackVpnSystem.getInstance().getWiFiKey(wifi);

            checkSavedNetwork(wifiKey, context);

//            Log.d(Const.LOG, "ChangeNetworkReceiver -> wifiKey " + wifiKey + " wifi state changed! "
//                    + info.getSupplicantState().name());

            // The end.

        } else {

            // Disconnect block
            SupplicantState currentState = SupplicantState.DISCONNECTED;
            if (lastState == currentState) {
                return; // because it is a repeated call
            } else {
                lastState = currentState;
            }
            Log.e(Const.LOG, "ChangeNetworkReceiver -> wifi has disabled");
        }
    }

    /**
     * Check the need for connection.
     *
     * If WiFi is a unknown network and we have the "isConnectOnUnknownWiFi" setting the app will
     * start connection.
     *
     * If WiFi is a known network and we have the "isConnectOnKnownWiFi" setting the app will start
     * connection.
     *
     * @param wifiKey current wifi key
     * @param context Context
     */
    private void checkSavedNetwork(String wifiKey, Context context) {
        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
        BlackVpnSystem system = BlackVpnSystem.getInstance();

        ArrayList<String> knownNetworks = system.getWiFiConfigurationList();

        if (prefs.isConnectOnUnknownWiFi() && !knownNetworks.contains(wifiKey)) {
            // UnKnown network
            Log.d(Const.LOG, "ChangeNetworkReceiver -> UnKnown network and we need to connect"
                    + ". key = " + wifiKey
            );
            startApp(context);

        } else if (prefs.isConnectOnKnownWiFi() && knownNetworks.contains(wifiKey)) {
            // Known network
            List<String> enabledKnownNetworks = BlackVpnSystem.getInstance()
                    .getListOfEnabledKnownWiFi();
            if (enabledKnownNetworks.contains(wifiKey)) {
                Log.d(Const.LOG, "ChangeNetworkReceiver -> Known network and we need to connect"
                        + ". key = " + wifiKey
                );

                startApp(context);
            }
        } else {
            // We don't need to connect
            Log.d(Const.LOG, "ChangeNetworkReceiver -> We don't need to connect"
                    + ". key = " + wifiKey
            );
        }
    }

    private void startApp(Context context) {

        BlackVpnSystem.getInstance().setForceStartForKnownWiFi(true);

        Intent intent = new Intent(context, Splash.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
