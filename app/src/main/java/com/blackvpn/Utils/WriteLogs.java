package com.blackvpn.Utils;

import android.content.Context;
import android.util.Log;

import com.blackvpn.Application;
import com.blackvpn.Events.AllEventVPN;
import com.blackvpn.Events.BusProvider;
import com.squareup.otto.Subscribe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.blinkt.openvpn.core.VpnStatus;

//import android.util.Log;

public class WriteLogs {
    private static BusProvider busProvider;
    private static WriteLogs thisObject;
    private String lastMessage = "";

    public WriteLogs() {
        // default constructor
    }

    private static WriteLogs logs;

    public static WriteLogs getInstance() {
        if (logs == null) {
            logs = new WriteLogs();
        }
        return logs;
    }

    public void initContext(Context context) {
        busProvider = BusProvider.getInstance();
        try {
            busProvider.register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        thisObject = this;
    }

    public static void unRegisterEventBus() {
        try {
            busProvider.unregister(thisObject);
        } catch (Exception e) {
            Log.e(Const.LOG, "WriteLogs.class -> unRegisterEventBus() - error - " + e.getMessage());
            Log.e(Const.LOG, "WriteLogs.class -> unRegisterEventBus() - error - " + e.getLocalizedMessage());
        }
    }

    public void cleanFile() {
        try {
            File file = new File(Application.getContext().getCacheDir(), Const.fileNameLogs);
            FileOutputStream outputStream = new FileOutputStream(file, false);
            writeAllText(outputStream, "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Subscribe
    public void allVPN(AllEventVPN eventVPN) {
        if (Application.getContext() != null) {
            String message = eventVPN.getLogItem().getString(Application.getContext());
            String time = getTime(eventVPN.getLogItem());
            message = time + message;
            if (!lastMessage.equals(message)) {
                lastMessage = message;
                Log.d(Const.LOG, "WriteLogs.class -> AllEventVPN() -> message: " + message);
//                message = readAllCachedText() + message;
                writeAllCachedText(message);
            }
        }

    }


    public static synchronized String readAllCachedText() {
        if (Application.getContext() != null) {
            File file = new File(Application.getContext().getCacheDir(), Const.fileNameLogs);
            return readAllText(file);
        }
        return "";
    }

    private static String readAllText(File file) {
        try {
            FileInputStream inputStream = new FileInputStream(file);
            return readAllText(inputStream);
        } catch (Exception ex) {
            return null;
        }
    }

    private static String readAllText(InputStream inputStream) {
        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);

        String line;
        StringBuilder text = new StringBuilder();

        try {
            while ((line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
//            Log.d(Const.LOG, "WriteLogs.class -> readAllText -> " + text);
        } catch (IOException e) {
            return null;
        } finally {
            try {
                inputreader.close();
                buffreader.close();
            } catch (IOException e) {
//                e.printStackTrace();
            }
        }
        return text.toString();
    }

    private synchronized boolean writeAllCachedText(String text) {
        if (Application.getContext() != null) {
            File file = new File(Application.getContext().getCacheDir(), Const.fileNameLogs);
            return writeAllText(file, text);
        }
        return false;
    }



    private boolean writeAllText(File file, String text) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file, true);
            return writeAllText(outputStream, text);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private boolean writeAllText(OutputStream outputStream, String text) {
        OutputStreamWriter outputWriter = new OutputStreamWriter(outputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(outputWriter);
        boolean success = false;

        try {
            bufferedWriter.write(text);
            bufferedWriter.append('\n');
            success = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                bufferedWriter.close();
                outputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return success;
    }

    private String getTime(VpnStatus.LogItem logItem) {
        Date d = new Date(logItem.getLogtime());
        java.text.DateFormat timeformat;
        timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        return timeformat.format(d) + " ";

    }


}
