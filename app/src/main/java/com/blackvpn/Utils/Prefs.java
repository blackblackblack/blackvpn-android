package com.blackvpn.Utils;


import net.orange_box.storebox.annotations.method.KeyByString;

/**
 * Shared Preferences
 *
 * version: 2
 */
public interface Prefs {
    @KeyByString("key_data_version")
    int getDataVersion();
    @KeyByString("key_data_version")
    void setDataVersion(int state);

    @KeyByString("key_status_vpn")
    String getStatusVPN();
    @KeyByString("key_status_vpn")
    void setStatusVPN(String state);

    @KeyByString("key_settings_user_name")
    void setUserName(String userName);
    @KeyByString("key_settings_user_name")
    String getUserName();

    @KeyByString("key_settings_password")
    void setPassword(String password);
    @KeyByString("key_settings_password")
    String getPassword();

    @KeyByString("key_settings_email")
    void setEmail(String email );
    @KeyByString("key_settings_email")
    String getEmail();

    @KeyByString("key_protocol_port")
    void setPort(String port );
    @KeyByString("key_protocol_port")
    String getPort();

    @KeyByString("key_protocol_protocol")
    void setProtocol(Boolean port);
    @KeyByString("key_protocol_protocol")
    Boolean getProtocol();

    @KeyByString("key_favorites_map")
    void setFavoritesGSON(String value);
    @KeyByString("key_favorites_map")
    String getFavoritesGSON();
    @KeyByString("key_favorites")
    String getFavoritesGSONV001();

    @KeyByString("key_gcm_token")
    String getGcmToken();
    @KeyByString("key_gcm_token")
    void setGcmToken(String gcmToken);

    @KeyByString("key_last_server")
    void setLastServer(Server.VPN server);
    @KeyByString("key_last_server")
    Server.VPN getLastServer();
    @KeyByString("key_last_server")
    String getLastServerV001();

    @KeyByString("key_autoconnect_vpn_location")
    Server.VPN getAutoConnectVpnLocation();
    @KeyByString("key_autoconnect_vpn_location")
    void setAutoConnectVpnLocation(Server.VPN server);

    @KeyByString("key_connect_on_known_wifi")
    boolean isConnectOnKnownWiFi();
    @KeyByString("key_connect_on_known_wifi")
    void setConnectOnKnownWiFi(boolean isConnectOnKnownWiFi);

    @KeyByString("key_connect_on_unknown_wifi")
    boolean isConnectOnUnknownWiFi();
    @KeyByString("key_connect_on_unknown_wifi")
    void setConnectOnUnknownWiFi(boolean isConnectOnUnknownWiFi);

    @KeyByString("key_connect_on_app_startup")
    boolean isConnectOnAppStartUp();
    @KeyByString("key_connect_on_app_startup")
    void setConnectOnAppStartUp(boolean isConnectOnAppStartUp);

    @KeyByString("key_start_app_on_device_startup")
    boolean isStartAppOnDeviceStartUp();
    @KeyByString("key_start_app_on_device_startup")
    void setStartAppOnDeviceStartUp(boolean isStartAppOnDeviceStartUp);

    @KeyByString("key_list_of_enabled_known_wifi")
    String getListOfEnabledKnownWifi();
    @KeyByString("key_list_of_enabled_known_wifi")
    void setListOfEnabledKnownWifi(String listOfKnownWifi);

    @KeyByString("key_saved_map_all_profiles")
    String getMapAllProfiles();
    @KeyByString("key_saved_map_all_profiles")
    void setMapAllProfiles(String gsonMap);

    @KeyByString("key_save_wifi_configuration")
    String getWiFiConfiguration();
    @KeyByString("key_save_wifi_configuration")
    void saveWiFiConfiguration(String gson);
}
