package com.blackvpn.Utils.Helpers;


import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.TextUtils;

public class GcmMessages {

    public static String getText(@NonNull String gcmMessage, @NonNull Context context) {

        if (TextUtils.isEmpty(gcmMessage)) return "";

        Resources res = context.getResources();
        int resId = res.getIdentifier(gcmMessage, "string", context.getPackageName());
        return resId == 0 ? gcmMessage : res.getString(resId);
    }

}
