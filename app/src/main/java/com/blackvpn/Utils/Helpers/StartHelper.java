package com.blackvpn.Utils.Helpers;

import android.content.Context;
import android.os.Handler;

import com.blackvpn.Activities.MainActivity;
import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;
import com.blackvpn.Utils.Server;
import com.blackvpn.Utils.WriteLogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ProfileManager;

public class StartHelper {
    private BlackVpnSystem system;
    private Prefs prefs;
    private MainActivity mainActivity;
    private ProfileManager profileManager;
    private Context context;

    public StartHelper() {
    }

    public void initSystem(Context ctx, BlackVpnSystem blackVpnSystem, MainActivity actv) {
        system = blackVpnSystem;
        this.context = ctx;
        prefs = system.getPrefs();
        this.mainActivity = actv;
        profileManager = ProfileManager.getInstance(context);
        WriteLogs.getInstance().initContext(ctx);
    }

    public void initProfiles() {
        //In the start, get all OpenVPN list of profiles. And save it in ArrayList.

        ArrayList<VpnProfile> vpnProfileArrayList = new ArrayList<>();
        Collection<VpnProfile> allVPN = profileManager.getProfiles();
        Iterator<VpnProfile> iterator = allVPN.iterator();

        while (iterator.hasNext()) {
            vpnProfileArrayList.add(iterator.next());
        }

        //if OpenVPN.list != BlackVPN.list - delete all and start new import
        HashMap<String, VpnProfile> mapAllProfiles = system.getMapAllProfiles();

        if (mapAllProfiles.size() == Server.VPN.values().length - 1) {

            for (Map.Entry<String, VpnProfile> entry : mapAllProfiles.entrySet()) {
                profileManager.addProfile(entry.getValue());
            }

            return;
        }

        if ((mapAllProfiles.size() == 0)
                || (mapAllProfiles.size() != vpnProfileArrayList.size())) {

            //delete all profiles
            for (int i = 0; i < vpnProfileArrayList.size(); i++) {
                profileManager.removeProfile(context, vpnProfileArrayList.get(i));
            }
            //create new profiles
            prefs.setStatusVPN(Const.VPN_STATE_NO_PROCESS);

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    createMapAllProfile();
                }
            });
        }
    }

    private void createMapAllProfile() {
        mainActivity.createMapAllProfile();
    }


}
