package com.blackvpn.Utils.Helpers;


import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;

import com.blackvpn.Activities.Splash;
import com.blackvpn.R;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Server;

import java.io.File;

public class BackgroundHelper {

    public interface BackgroundForServer {
        void setBackgroundForCurrentServer(CoordinatorLayout coordinatorLayout,
                                           Context context, Server.VPN server);
    }

    private BackgroundHelper() {}



    public static Drawable getBackground(AppCompatActivity activity, Server.VPN server) {

        if (server == null) {
            server = Server.VPN.UNDEFINED;
        }

        int activityOrientation = activity.getResources().getConfiguration().orientation;

        int resId = Configuration.ORIENTATION_PORTRAIT == activityOrientation ?
                server.getPortrait() : server.getLandscape();

        //Get the dimensions of the Screen
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;

        return decodeBitmapFromResource(activity.getResources(), resId, screenWidth, screenHeight);
    }


    private static Drawable decodeBitmapFromResource(Resources res, int resId,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate size
        options.inSampleSize = calculateSize(options, reqWidth, reqHeight);

        // Decode bitmap with calculateSize set
        options.inJustDecodeBounds = false;

        return new BitmapDrawable(res, BitmapFactory.decodeResource(res, resId, options));
    }



    private static int calculateSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int sizeScale = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest sizeScale value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / sizeScale) >= reqHeight
                    && (halfWidth / sizeScale) >= reqWidth) {
                sizeScale *= 2;
            }
        }

        return sizeScale;
    }
}
