package com.blackvpn.Utils.VPNwork;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;

import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;

public class Disconnect2 {

    protected static OpenVPNService mService;
    private static Context context;
    private static boolean isBound = false;

    private static ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
//            Log.i(Const.LOG, "Disconnect2.class -> ServiceConnection.class -> onServiceConnected()");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            OpenVPNService.LocalBinder binder = (OpenVPNService.LocalBinder) service;
            mService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }

    };

    public static void start(Context ctx) {
//        Log.i(Const.LOG, "Disconnect2.class -> start");
        context = ctx;
        Intent intent = new Intent(context, OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        isBound = context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
//                Log.i(Const.LOG, "Disconnect2.class -> new Thread() - closeService()");
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                closeService();
            }
        });

        thread.start();
    }


    private static void closeService() {
//        Log.i(Const.LOG, "Disconnect2.class -> closeService()");
        ProfileManager.setConntectedVpnProfileDisconnected(context);

        if (mService != null && mService.getManagement() != null) {
//            Log.i(Const.LOG, "Disconnect2.class -> closeService() - mService.getManagement().stopVPN() ");
            mService.getManagement().stopVPN();
        }

        BlackVpnSystem system = BlackVpnSystem.getInstance();
        BlackVpnSystem.setVpnEvent(false);
        system.getPrefs().setStatusVPN(Const.VPN_STATE_NO_PROCESS);
        system.setIsBlocked(true);

        if (mConnection != null) {
            try {
                if (isBound){
                    context.unbindService(mConnection);
                    isBound = false;
                }
            } catch (IllegalArgumentException e){
                e.printStackTrace();
            }
        }
    }
}
