package com.blackvpn.Utils.VPNwork;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;

import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;

public class Disconnect extends Activity implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
    protected OpenVPNService mService;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            Log.i(Const.LOG, "Disconnect.class -> ServiceConnection.class -> onServiceConnected()");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            OpenVPNService.LocalBinder binder = (OpenVPNService.LocalBinder) service;
            mService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }

    };

    @Override
    protected void onResume() {
        Log.i(Const.LOG, "Disconnect.class -> onResume()");
        super.onResume();
        Intent intent = new Intent(this, OpenVPNService.class);
        intent.setAction(OpenVPNService.START_SERVICE);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        Log.i(Const.LOG, "Disconnect.class -> onResume() - go to show dialog");

//        showDisconnectDialog();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(Const.LOG, "Disconnect.class -> new Thread() - closeService()");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                closeService();
            }
        });

        thread.start();
    }

    @Override
    protected void onPause() {
        Log.i(Const.LOG, "Disconnect.class -> onPause()");
        super.onPause();
        unbindService(mConnection);
    }

    private void showDisconnectDialog() {
        Log.i(Const.LOG, "Disconnect.class -> showDisconnectDialog()");
        final Context ctx = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(de.blinkt.openvpn.R.string.title_cancel);
        builder.setMessage(de.blinkt.openvpn.R.string.cancel_connection_query);
        builder.setNegativeButton(android.R.string.no, this);
        builder.setPositiveButton(android.R.string.yes, this);
        builder.setOnCancelListener(this);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
           closeService();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        finish();
    }

    private void closeService() {
        Log.i(Const.LOG, "Disconnect.class -> closeService()");
        ProfileManager.setConntectedVpnProfileDisconnected(this);

        if (mService != null && mService.getManagement() != null) {
            Log.i(Const.LOG, "Disconnect.class -> closeService() - mService.getManagement().stopVPN() ");
            mService.getManagement().stopVPN();
        }

        BlackVpnSystem system = BlackVpnSystem.getInstance();
        BlackVpnSystem.setVpnEvent(false);
        system.getPrefs().setStatusVPN(Const.VPN_STATE_NO_PROCESS);
        system.setIsBlocked(true);

        finish();
    }
}
