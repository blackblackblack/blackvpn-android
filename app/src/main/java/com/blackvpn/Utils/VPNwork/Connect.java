package com.blackvpn.Utils.VPNwork;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.blackvpn.Utils.BlackVpnSystem;
import com.blackvpn.Utils.Const;
import com.blackvpn.Utils.Prefs;

import java.util.HashMap;

import de.blinkt.openvpn.LaunchVPN;
import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ProfileManager;

public class Connect {
    private Context context;
    private BlackVpnSystem system;
    private Prefs prefs;

    public Connect(Context ctx) {
        this.context = ctx;
        system = BlackVpnSystem.getInstance();
        prefs = system.getPrefs();
    }

    public Const.Signal startVPN(String serverKey, Activity activity) {

        //prepare VPN.Profile. If is error = return Disconnected.
        HashMap<String, VpnProfile> mapAllProfiles = system.getMapAllProfiles();
        BlackVpnSystem.setVpnEvent(true); //init EventBus
        system.setIsBlocked(false); //unblocked save status
        VpnProfile vpnProfile = null;
        try {
//            String nameServer = Const.COUNTRIES[position];
            vpnProfile = mapAllProfiles.get(serverKey);
        } catch (Exception e) {
            Log.e(Const.LOG, e.getLocalizedMessage());
        }
        if (vpnProfile == null) return Const.Signal.Disconnected;

        //check name for this Vpn.Profile
        String nameProfile = vpnProfile.mName;
        String uuID = vpnProfile.getUUID().toString();
        if (TextUtils.isEmpty(ProfileManager.get(context, uuID).getName())) {
            if (!TextUtils.isEmpty(nameProfile)) {
                ProfileManager.get(context, uuID).mName = nameProfile;
            } else return Const.Signal.Disconnected;
        }


        //prepare port, protocol, name and password
        vpnProfile.mServerPort = prefs.getPort();
        vpnProfile.mUseUdp = !prefs.getProtocol();
        vpnProfile.mUsername = prefs.getUserName();
        vpnProfile.mPassword = prefs.getPassword();

        //if email or password is null - return a signal
        if (TextUtils.isEmpty(vpnProfile.mUsername) || TextUtils.isEmpty(vpnProfile.mPassword)) {
            return Const.Signal.NoEmailOrPassword;
        } else {
            //clean logs file
            system.getLogs().cleanFile();

            //startConnect
            Intent intent = new Intent(activity, LaunchVPN.class);
            intent.putExtra(LaunchVPN.EXTRA_KEY, vpnProfile.getUUID().toString());
            intent.setAction(Intent.ACTION_MAIN);
            activity.startActivity(intent);
            return Const.Signal.Connecting;
        }
    }

}