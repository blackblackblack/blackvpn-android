package com.blackvpn.Utils;

import com.blackvpn.R;

import java.util.HashMap;

public class Const {
    public static final String LOG = "BlackVPN";
    public static final Boolean UDP = false;
    public static final Boolean TCP = true;

    public static final String VPN_STATE_NO_PROCESS = "NOPROCESS";
    public static final String VPN_STATE_CONNECTED = "CONNECTED";
    public static final String VPN_STATE_UNKNOWN_LEVEL = "UNKNOWN_LEVEL";
    public static final String USER_VPN_PERMISSION_CANCELLED = "USER_VPN_PERMISSION_CANCELLED";
    public static final String EXITING = "EXITING";
    public static final String EXECUTE_FORCE_EXIT = "execute_force_exit";

    // post request ticket
    public static final String TICKET_API_KEY = "947467B0794D3C82DE32F252FDBF1707";
    public static  final String TICKET_URL = "https://support-api.blackvpn.com";

    public static final String [] DROP_DOWN_SPINNER_TICKET = new String[] {
            "Setup & Connect", "Billing & Refunds", "Other Questions"
    };


    public static final String DEFAULT_PORT = "443";

    public static final int IMPORT_PROFILE = 231;
    public static final int SELECT_SERVERS = 331;
    static final String fileNameLogs = "cacheLogs.txt";
    public static final String AUTH_FAILED = "AUTH_FAILED";
    public static final String USA = "USA";
    public static final String UK = "UK";
    public static final String ALL = "ALL";

    public enum Signal {Connecting, Connected, Disconnected, NoEmailOrPassword;}

    public static final String FAQ_URL = "https://support.blackvpn.com/kb/index.php";
    public static final String LOST_USER_NAME_URL = "https://www.blackvpn.com/support/lost-username/";
    public static final String LOST_USER_PASSWORD_URL = "https://www.blackvpn.com/support/lost-password/";
    public static final String SHOP_BUY_PRIVACY = "https://www.blackvpn.com/checkout/privacy/";
    public static final String SHOP_BUY_GLOBAL = "https://www.blackvpn.com/checkout/global/";
    public static final String SHOP_BUY_TV = "https://www.blackvpn.com/checkout/tv/";
    public static final String SHOP_BUY_USA = "https://www.blackvpn.com/checkout/us/";
    public static final String SHOP_BUY_UK = "https://www.blackvpn.com/checkout/uk/";
    public static final String TLS_ERROR = "tls-error,,";
    public static final String RECONNECTING = "RECONNECTING";
    public static final String LEVEL_CONNECTING_NO_SERVER_REPLY_YET = "LEVEL_CONNECTING_NO_SERVER_REPLY_YET";
    public static final String GCM_RECEIVED_MESSAGE = "GCM_RECEIVED_MESSAGE";
    public static final String GCM_UPDATE_TOKEN = "GCM_UPDATE_TOKEN";
    public static final String GCM_ERROR = "GCM_ERROR";
    public static final String GCM_MESSAGE_WITH_ERROR = "GCM_MESSAGE_WITH_ERROR";

}
