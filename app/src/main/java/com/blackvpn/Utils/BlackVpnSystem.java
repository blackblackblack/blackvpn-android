package com.blackvpn.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.util.Log;
import android.view.MenuItem;

import com.blackvpn.Activities.AccountActivity;
import com.blackvpn.Activities.AutoConnectActivity;
import com.blackvpn.Activities.BuyNowActivity;
import com.blackvpn.Activities.ProtocolActivity;
import com.blackvpn.Activities.SupportActivity;
import com.blackvpn.Activities.TermsActivity;
import com.blackvpn.Application;
import com.blackvpn.Events.VpnEvent;
import com.blackvpn.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.orange_box.storebox.StoreBox;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.blinkt.openvpn.VpnProfile;

public class BlackVpnSystem {

    public static final int DATA_VERSION = 2;

    private static BlackVpnSystem system;
    private static Prefs prefs;
    private boolean isBlocked = true;
    private volatile HashMap<String, VpnProfile> mapAllProfiles;
    private static VpnEvent vpnEvent;
    private static boolean isForceStartForKnownWiFi;

    private boolean appIsNowStarting;

    public static void setVpnEvent(boolean isWork) {
        if (!isWork && (vpnEvent != null)) {
            vpnEvent.removeListeners();
            vpnEvent = null;
        } else {
            vpnEvent = new VpnEvent();
        }
    }

    private BlackVpnSystem() {
        // There is a private default constructor.
        getPrefs();
    }

    public static BlackVpnSystem getInstance() {
        if (system == null){
            system = new BlackVpnSystem();
            system.appIsNowStarting = true;
        }
        return system;
    }

    public synchronized HashMap<String, VpnProfile> getMapAllProfiles() {
        if (mapAllProfiles == null) {
            mapAllProfiles = getSavedMapAllProfiles();
        }

        return mapAllProfiles;
    }

    private HashMap<String, VpnProfile> getSavedMapAllProfiles() {
        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();

        Type mapType = new TypeToken<Map<String, VpnProfile>>() {}.getType();
        String gson = prefs.getMapAllProfiles();

        HashMap<String, VpnProfile> result = new HashMap<>();

        Map<String, Object> savedMap = new Gson().fromJson(gson, mapType);
        if (savedMap == null) {
            return result;
        }

        for (Map.Entry<String, Object> entry : savedMap.entrySet()) {
            result.put(entry.getKey(), (VpnProfile) entry.getValue());
        }

        return result;
    }

    public synchronized void setMapAllProfiles(Map<String, VpnProfile> map) {
        mapAllProfiles = (HashMap<String, VpnProfile>) map;

        // save only if it is a full array
        if (mapAllProfiles.size() == Server.VPN.values().length - 1) {
            Type mapType = new TypeToken<Map<String, VpnProfile>>() {}.getType();
            String gsonMap = new Gson().toJson(map, mapType);
            getPrefs().setMapAllProfiles(gsonMap);
        }
    }


    public synchronized Prefs getPrefs() {
        if (prefs == null) {
            prefs = StoreBox.create(Application.getContext(), Prefs.class);
        }
        return prefs;
    }

    public WriteLogs getLogs() {
        return WriteLogs.getInstance();
    }

    public Intent onToolbarItemClick(Activity activity, MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.activity_buynow:
                intent = new Intent(activity, BuyNowActivity.class);
                break;
//            case R.id.activity_speed_vpn:
//                intent = new Intent(activity, VPNSpeedTestActivity.class);
//                break;
            case R.id.activity_account:
                intent = new Intent(activity, AccountActivity.class);
                break;
            case R.id.activity_autoconnect:
                intent = new Intent(activity, AutoConnectActivity.class);
                break;
            case R.id.activity_protocol:
                intent = new Intent(activity, ProtocolActivity.class);
                break;
            case R.id.activity_terms:
                intent = new Intent(activity, TermsActivity.class);
                break;
            case R.id.activity_support:
                intent = new Intent(activity, SupportActivity.class);
                break;
//            case R.id.activity_help:
//                intent = new Intent(activity, HelpActivity.class);
//                break;
        }

        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        }
        return intent;
    }


    public boolean isNetworkConnected() {
        if (Application.getContext() != null) {
            ConnectivityManager cm = (ConnectivityManager) Application.getContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        }
        return false;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }


    /**
     * Get a list of ENABLED known wifi networks to the Shared Preferences
     *
     * @return List of strings with custom keys of networks
     */
    public ArrayList<String> getListOfEnabledKnownWiFi() {
        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
        Type listType = new TypeToken<List<String>>() {}.getType();
        List<String> listOfKnownWifi = new Gson().fromJson(prefs.getListOfEnabledKnownWifi(), listType);
        return listOfKnownWifi == null ? new ArrayList<String>() : (ArrayList<String>)listOfKnownWifi;
    }

    /**
     * Save a list of ENABLED known wifi networks to the Shared Preferences
     *
     * @param listOfKnownWifi List of strings with custom keys of networks
     */
    public void setListOfEnabledKnownWiFi(List<String> listOfKnownWifi) {
        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
        String gson = new Gson().toJson(listOfKnownWifi);
        prefs.setListOfEnabledKnownWifi(gson);
        prefs.setConnectOnKnownWiFi(listOfKnownWifi != null && listOfKnownWifi.size() > 0);
    }

    /**
     * Make custom key from wifi configuration
     *
     * @param wifi WifiConfiguration
     * @return String custom key
     */
    public String getWiFiKey(WifiConfiguration wifi) {
        String ssid = wifi.SSID.replaceAll("\"","");
        return "networkId=" + wifi.networkId + ",SSID=" + ssid;
    }

    public void setNowAppIsStarting(boolean isStarting) {
        appIsNowStarting = isStarting;
    }

    public boolean appIsNowStarting() {
        return appIsNowStarting;
    }

    public void saveWiFiConfigurationList(List<WifiConfiguration> wifiConfigurationList) {
        List<String> result = new ArrayList<>();

        if (wifiConfigurationList == null) return;

        for (WifiConfiguration wifi : wifiConfigurationList) {
            result.add(getWiFiKey(wifi)); // save key of wifi
        }

        String gson = new Gson().toJson(result);
        getPrefs().saveWiFiConfiguration(gson);
    }

    public ArrayList<String> getWiFiConfigurationList() {
        Type listType = new TypeToken<List<String>>() {}.getType();
        List<String> result = new Gson().fromJson(getPrefs().getWiFiConfiguration(), listType);
        return result == null ? new ArrayList<String>() : (ArrayList<String>) result;
    }

    public void setForceStartForKnownWiFi(boolean isForceStart) {
        isForceStartForKnownWiFi = isForceStart;
    }

    public boolean isForceStartForKnownWiFi() {
        return isForceStartForKnownWiFi;
    }

    /**
     * Data migration
     */
    public void migrate() {
        if (prefs.getDataVersion() != DATA_VERSION) {
            // First run or data version is 1
            if (prefs.getDataVersion() == 0) {
                migrateV001toV002();
                prefs.setDataVersion(2);
            }
            // Version is 2
            //if (prefs.getDataVersion() == 2) {
            //    migrateV002toV003();
            //    prefs.setDataVersion(3);
            //}
        }
    }

    private void migrateV001toV002() {
        // Last server
        prefs.setLastServer(Server.VPN.getServerFromKey(prefs.getLastServerV001()));
        // Favorite servers
        String oldFavorites = prefs.getFavoritesGSONV001() != null ? prefs.getFavoritesGSONV001() : "[]";
        Map<Server.VPN, Boolean> savedMap = new HashMap();
        for (Server.VPN vpn : Server.VPN.values()) {
            savedMap.put(vpn, oldFavorites.contains(vpn.getProfileKey()));
        }
        //savedMap.put(Server.getDefaultServer(), true);
        Server.saveFavorites(savedMap);
    }

}
