package com.blackvpn.Utils;

import android.content.res.Resources;
import android.util.Log;

import com.blackvpn.Application;
import com.blackvpn.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Constants of servers list.
 *
 * @author Andrew.Gahov@gmail.com  (08.05.17)
 */

public class Server {

    public static final String PROFILE_KEY = "PROFILE_KEY";
    private static HashMap<VPN, Boolean> mFavorites;
    private static Server.VPN[] allVpnList;

    public static VPN getDefaultServer() {
        return VPN.NETHERLANDS;
    }

    private static final int normalIcon = R.drawable.cloud_on;
    private static final int tvIcon = R.drawable.youtube_on;
    private static final String PATH_RESOURCES = "android.resource://com.blackvpn/";

    public static VPN[] getAllVpnList() {
        if (allVpnList == null) {
            allVpnList = getSortedList(Application.getContext().getResources());
        }
        return allVpnList;
    }

    public static Server.VPN[] getSortedList(Resources res) {
        ArrayList<String> sortedCountries = new ArrayList<>();
        Server.VPN[] allVpns = Server.VPN.values();
        HashMap<String, VPN> map = new HashMap<>();

        // Get all titles
        for (Server.VPN server : allVpns) {

            if (Server.VPN.UNDEFINED == server) continue; // without UNDEFINED server

            String countryName = res.getString(server.getTitle());
            sortedCountries.add(countryName);
            map.put(countryName, server);
        }


        // Sorting ! It is an important step.
        Collections.sort(sortedCountries);

        Server.VPN[] result = new Server.VPN[sortedCountries.size()];
        for (int i = 0; i < sortedCountries.size(); i++) {
            String sortedValue = sortedCountries.get(i);
            result[i] = map.get(sortedValue);
        }

        return result;
    }

    public static boolean isServerFavorite(VPN server) {
        if (mFavorites == null) {
            mFavorites = getSavedFavorites();
        }
        return mFavorites.containsKey(server) && mFavorites.get(server);
    }

    public static void setFavoriteServer(VPN server, boolean isFavorite) {
        if (mFavorites == null) {
            mFavorites = getSavedFavorites();
        }
        mFavorites.put(server, isFavorite);
        saveFavorites(mFavorites);
    }

    public static ArrayList<VPN> getFavoritesList() {
        ArrayList<VPN> result = new ArrayList<>();

        if (mFavorites == null) {
            mFavorites = getSavedFavorites();
        }

        VPN[] vpns = getSortedList(Application.getContext().getResources());
        for (VPN vpn : vpns) {
            boolean isFavorite = mFavorites.get(vpn);
            if (isFavorite) {
                result.add(vpn);
            }
        }

        return result;
    }


    private static HashMap<Server.VPN, Boolean> getSavedFavorites() {
        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
        Type itemsMapType = new TypeToken<Map<VPN, Boolean>>() {}.getType();
        Map<Server.VPN, Boolean> savedMap = new Gson().fromJson(prefs.getFavoritesGSON(), itemsMapType);
        if (savedMap == null) {
            savedMap = new HashMap<>();
            for (VPN vpn : VPN.values()) {
                savedMap.put(vpn, false);
            }
            savedMap.put(getDefaultServer(), true);
            saveFavorites(savedMap);
        }
        return (HashMap<Server.VPN, Boolean>) savedMap;
    }

    public static void saveFavorites(Map<Server.VPN, Boolean> map) {
        Prefs prefs = BlackVpnSystem.getInstance().getPrefs();
        String gson = new Gson().toJson(map);
        prefs.setFavoritesGSON(gson);
    }



    public enum VPN {
        AUSTRALIA(R.drawable.australia_port, R.drawable.australia_land, R.string.australia, "australia", R.raw.australia, false),
        BRAZIL(R.drawable.brazil_port, R.drawable.brazil_land, R.string.brazil, "brazil", R.raw.brazil, false),
        CANADA(R.drawable.canada_port, R.drawable.canada_land, R.string.canada, "canada", R.raw.canada, false),
        CZECH(R.drawable.czech_port, R.drawable.czech_land, R.string.czech, "czech", R.raw.czech, false),
        ESTONIA(R.drawable.estonia_port, R.drawable.estonia_land, R.string.estonia, "estonia", R.raw.estonia, false),
        FRANCE(R.drawable.france_port, R.drawable.france_land, R.string.france, "france", R.raw.france, false),
        GERMANY(R.drawable.germany_port, R.drawable.germany_land, R.string.germany, "germany", R.raw.germany, false),
        LITHUANIA(R.drawable.lithuania_port, R.drawable.lithuania_land, R.string.lithuania, "lithuania", R.raw.lithuania, false),
        LUXEMBOURG(R.drawable.luxembourg_port, R.drawable.luxembourg_land, R.string.luxembourg, "luxembourg", R.raw.luxembourg, false),
        NETHERLANDS(R.drawable.netherlands_port, R.drawable.netherlands_land, R.string.netherlands, "netherlands", R.raw.netherlands, false),
        NORWAY(R.drawable.norway_port, R.drawable.norway_land, R.string.norway, "norway", R.raw.norway, false),
        ROMANIA(R.drawable.romania_port, R.drawable.romania_land, R.string.romania, "romania", R.raw.romania, false),
        RUSSIA(R.drawable.russia_port, R.drawable.russia_land, R.string.russia, "russia", R.raw.russia, false),
        SPAIN(R.drawable.spain_port, R.drawable.spain_land, R.string.spain, "spain", R.raw.spain, false),
        SWITZERLAND(R.drawable.switzerland_port, R.drawable.switzerland_land, R.string.switzerland, "switzerland", R.raw.switzerland, false),
        UKRAINE(R.drawable.ukraine_port, R.drawable.ukraine_land, R.string.ukraine, "ukraine", R.raw.ukraine, false),
        UNITED_KINGDOM(R.drawable.united_kingdom_port, R.drawable.united_kingdom_land, R.string.united_kingdom, "united_kingdom", R.raw.united_kingdom, true),
        USA_CENTRAL(R.drawable.usa_central_port, R.drawable.usa_central_land, R.string.usa_central, "usa_central", R.raw.usa_central, true),
        EAST_COAST(R.drawable.east_coast_port, R.drawable.east_coast_land, R.string.east_coast, "east_coast", R.raw.east_coast, true),
        WEST_COAST(R.drawable.west_coast_port, R.drawable.west_coast_land, R.string.west_coast, "west_coast", R.raw.west_coast, true),
        JAPAN(R.drawable.japan_port, R.drawable.japan_land, R.string.japan, "japan", R.raw.japan, false),
        UNDEFINED(R.drawable.overall, R.drawable.overall_land, R.string.undefined, "undefined", R.raw.netherlands, false);

        private final int portrait;
        private final int landscape;
        private final int title;
        private final boolean isTvServer;
        private final String profileKey;
        private final int raw;

        VPN(int portrait, int landscape, int title, String profileKey, int raw, boolean isTvServer) {
            this.portrait = portrait;
            this.landscape = landscape;
            this.title = title;
            this.profileKey = profileKey;
            this.raw = raw;
            this.isTvServer = isTvServer;
        }

        public int getPortrait() {
            return portrait;
        }

        public int getLandscape() {
            return landscape;
        }

        public int getTitle() {
            return title;
        }

        public boolean isTvServer() {
            return isTvServer;
        }

        public int getServerIcon() {
            return isTvServer() ? tvIcon : normalIcon;
        }

        public String getFilePath() {
            return PATH_RESOURCES + raw;
        }

        public String getProfileKey() {
            return profileKey;
        }

        public static VPN getServerFromKey(String profileKey) {
            for (VPN vpn : getAllVpnList()) {
                if (vpn.getProfileKey().equals(profileKey)) {
                    return vpn;
                }
            }
            return getDefaultServer();
        }
    }
}
